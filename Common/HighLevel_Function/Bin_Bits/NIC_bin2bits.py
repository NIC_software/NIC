import os
import struct
import numpy as np
import torch
import torch.nn as nn
from Common.models.NIC_models.acceleration_utils import pack_bools
from Common.models.NIC_models.context_model import Demultiplexerv2
from Common.models.NIC_models.factorized_entropy_model import Entropy_bottleneck
from Common.models.NIC_models.hyper_module import h_synthesisTransform
from Common.models.NIC_models.functional_class import NormalCDF
import rans

from Common.models.NIC_models.quantization import *
from Common.models.NIC_models.quantization.quant_registry import nic_quantize_class

@nic_quantize_class
class NIC_Bin2Bits(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        coding_tool = header.coding_tool
        # M, N2 = header.M, header.N2
        M, N2 = 192, 128
        if header.model_id < 2:
            lambda_rd_nom_used = header.lambda_rd_nom_scaled / pow(2, 16) * 1.2
            lambda_rd_numpy = np.zeros((1, 1), np.float32)
            lambda_rd_numpy[0, 0] = lambda_rd_nom_used
            header.lambda_rd = torch.Tensor(lambda_rd_numpy)
            if coding_tool.multi_hyper_flag:
                if coding_tool.predictor_flag:
                    self.factorized_entropy_func = Entropy_bottleneck(128)
            else:
                self.factorized_entropy_func = Entropy_bottleneck(N2)
        else:
            if coding_tool.multi_hyper_flag:
                self.factorized_entropy_func = Entropy_bottleneck(128)
            else:
                self.factorized_entropy_func = Entropy_bottleneck(N2)
            header.lambda_rd = None

        # TODO: already have header.M and header.N2
        self.c_main = M
        
        if coding_tool.multi_hyper_flag:
            self.c_hyper = 256
            self.c_hyper_2 = 128
        else:
            self.c_hyper = N2
        self.softmax = nn.Softmax(dim=-1)
        self.normal_cdf = NormalCDF()

    def encode(self, bits2bin_varlist, picture, file_object, Block_Idx):
        ## added extra dirs for temporary bin, then delete
        bin_dir_path = os.path.dirname(file_object.name)  ## bin 所在目录
        print("bin_dir_path: ", bin_dir_path)
        header = picture.picture_header

        # Main Arith Encode
        y_main_q, y_hyper_q, params_prob = bits2bin_varlist[0], bits2bin_varlist[1], bits2bin_varlist[2]
        device = y_main_q.device
        channel_num = y_main_q.shape[1]
        y_main_q = torch.cat((torch.cat(Demultiplexerv2(y_main_q[:, :channel_num // 2, :, :]), dim=1),
                             torch.cat(Demultiplexerv2(y_main_q[:, channel_num // 2:, :, :]), dim=1)), dim=1)

        prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = [
            torch.chunk(params_prob, 9, dim=1)[i].squeeze(1) for i in range(9)]

        #calculate threshold
        threshold = 0.25 - 0.5 ** 9
        tmp = 0
        for search in range(7):
             not_sparsity = (torch.abs(scale0) > threshold) | (
                     torch.abs(scale1) > threshold) | (
                                    torch.abs(scale2) > threshold) | (
                                    torch.abs(mean0) > threshold) | (
                                    torch.abs(mean1) > threshold) | (
                                    torch.abs(mean2) > threshold)

             delta = torch.mean((y_main_q - y_main_q * not_sparsity) ** 2)
             if delta.item() == 0:
                 tmp = threshold + 0.0
                 threshold += 1. / (2 ** (search + 3))
             else:
                 threshold -= 1. / (2 ** (search + 3))
        threshold = tmp + 0.0

        not_sparsity = (torch.abs(scale0) > threshold) | (
        torch.abs(scale1) > threshold) | (
                                torch.abs(scale2) > threshold) | (
                                torch.abs(mean0) > threshold) | (
                                torch.abs(mean1) > threshold) | (
                                torch.abs(mean2) > threshold)

        Datas = torch.reshape(y_main_q, [-1]).cpu().numpy().astype(np.int).tolist()
        _, c, h, w = y_main_q.shape

        max_main = max(Datas)
        min_main = min(Datas)
        sample = np.arange(min_main, max_main+1+1)  # [Min_V - 0.5 , Max_V + 0.5]

        not_sparsity = not_sparsity.view(len(Datas)).detach().cpu().numpy()
        Effective_Datas = np.array(Datas)[not_sparsity].tolist()
        if len(Effective_Datas)>0:
            params_prob = params_prob.view(1, 9, -1)
            params_prob = params_prob.detach().cpu().numpy()[:, :, not_sparsity]
            params_prob = torch.from_numpy(params_prob).to(device)

            sample = torch.FloatTensor(np.tile(sample, [1, len(Effective_Datas), 1])).to(device)

            # 3 gaussian
            prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = [
                torch.chunk(params_prob, 9, dim=1)[i].squeeze(1) for i in range(9)]
            del params_prob
            # keep the weight summation of prob == 1
            probs = torch.stack([prob0, prob1, prob2], dim=-1)
            del prob0, prob1, prob2

            probs = self.softmax(probs)
            # process the scale value to positive non-zero
            scale0 = torch.abs(scale0)
            scale1 = torch.abs(scale1)
            scale2 = torch.abs(scale2)
            scale0[scale0 < 1e-6] = 1e-6
            scale1[scale1 < 1e-6] = 1e-6
            scale2[scale2 < 1e-6] = 1e-6
            mean0 = torch.unsqueeze(mean0,dim=2)
            mean1 = torch.unsqueeze(mean1, dim=2)
            mean2 = torch.unsqueeze(mean2, dim=2)
            scale0 = torch.unsqueeze(scale0, dim=2)
            scale1 = torch.unsqueeze(scale1, dim=2)
            scale2 = torch.unsqueeze(scale2, dim=2)

            lower = torch.zeros(1, len(Effective_Datas), max_main - min_main + 2)
            lower0 = self.normal_cdf(sample - 0.5, scale0, mean0)
            lower1 = self.normal_cdf(sample - 0.5, scale1, mean1)
            lower2 = self.normal_cdf(sample - 0.5, scale2, mean2)
            for i in range(sample.shape[2]):
                 lower[:, :, i] = probs[:, :, 0] * lower0[:, :, i] + \
                                  probs[:, :, 1] * lower1[:, :, i] + probs[:, :, 2] * lower2[:, :, i]


            del probs, lower0, lower1, lower2

            precise = 16
            cdf_m = lower.data.cpu().numpy()*((1 << precise) - (max_main -
                                                                min_main + 1))  # [1, c, h, w ,Max-Min+1]
            cdf_m = cdf_m.astype(np.int32) + sample.cpu().numpy().astype(np.int32) - min_main


            cdf_main = np.reshape(cdf_m, [len(Effective_Datas), -1])
            Cdf_lower_main = np.fromiter(map(lambda x, y: int(y[x - min_main]), Effective_Datas, cdf_main), dtype=np.int32)
            Cdf_upper_main = np.fromiter(map(lambda x, y: int(y[x - min_main]), Effective_Datas, cdf_main[:, 1:]), dtype=np.int32)

        if header.coding_tool.multi_hyper_flag:
            y_hyper_2_q, hyper_2_dec = bits2bin_varlist[3], bits2bin_varlist[4]
            # Hyper 1 Arith Encode
            Datas = torch.reshape(y_hyper_q, [-1]).cpu().numpy().astype(np.int)
            max_hyper_1 = max(Datas)
            min_hyper_1 = min(Datas)
            sample = np.arange(min_hyper_1, max_hyper_1+1+1)  # [Min_V - 0.5 , Max_V + 0.5]
            _, c, h, w = y_hyper_q.shape
            sample = torch.FloatTensor(np.tile(sample, [1, c, h, w, 1])).to(device)

            mean = hyper_2_dec[:, :c, :, :]
            scale = hyper_2_dec[:, c:, :, :]

            scale = torch.abs(scale)
            scale[scale < 1e-6] = 1e-6

            lower = torch.zeros(1, c, h, w, max_hyper_1-min_hyper_1+2).to(device)
            for ii in range(sample.shape[4]):
                lower[:,:,:,:,ii] = self.normal_cdf(sample[:,:,:,:,ii]-0.5, scale, mean)
            precise = 16
            cdf_m = lower.data.cpu().numpy()*((1 << precise) - (max_hyper_1 -
                                                                min_hyper_1 + 1))  # [1, c, h, w ,Max-Min+1]
            cdf_m = cdf_m.astype(np.int32) + sample.cpu().numpy().astype(np.int32) - min_hyper_1
            cdf_main = np.reshape(cdf_m, [len(Datas), -1])

            # Cdf[Datas - Min_V]
            Cdf_lower_hyper1 = np.fromiter(map(lambda x, y: int(y[x - min_hyper_1]), Datas, cdf_main), dtype=np.int32)
            # Cdf[Datas + 1 - Min_V]
            Cdf_upper_hyper1 = list(map(lambda x, y: int(
                y[x - min_hyper_1]), Datas, cdf_main[:, 1:]))

            # Hyper 2 Arith Encode
            min_hyper_2 = torch.min(y_hyper_2_q).cpu().numpy().astype(np.int)
            max_hyper_2 = torch.max(y_hyper_2_q).cpu().numpy().astype(np.int)
            _, c, h, w = y_hyper_2_q.shape
            # print("Hyper Channel:", c)
            Datas_hyper = torch.reshape(
                y_hyper_2_q, [c, -1]).cpu().numpy().astype(np.int)
            # [Min_V - 0.5 , Max_V + 0.5]
            # sample = np.arange(min_hyper_2, max_hyper_2+1+1)
            # sample = np.tile(sample, [c, 1, 1])
            # lower = torch.sigmoid(self.factorized_entropy_func._logits_cumulative(
            #     torch.FloatTensor(sample).to(device) - 0.5, stop_gradient=False))

            # cdf_h = lower.data.cpu().numpy()*((1 << precise) - (max_hyper_2 -
            #                                                     min_hyper_2 + 1))  # [N1, 1, Max-Min+1]
            # cdf_h = cdf_h.astype(np.int) + sample.astype(np.int) - min_hyper_2
            cdf_h = self.factorized_entropy_func.make_cdf(min_hyper_2, max_hyper_2)
            cdf_hyper = np.reshape(np.tile(cdf_h, [len(Datas_hyper[0]), 1, 1, 1]), [
                                len(Datas_hyper[0]), c, -1])

            # Datas_hyper [256, N], cdf_hyper [256,1,X]
            Cdf_0, Cdf_1 = [], []
            for i in range(c):
                Cdf_0.extend(np.fromiter(map(lambda x, y: int(
                    y[x - min_hyper_2]), Datas_hyper[i], cdf_hyper[:, i, :]), dtype=np.int32))   # Cdf[Datas - Min_V]
                Cdf_1.extend(np.fromiter(map(lambda x, y: int(
                    y[x - min_hyper_2]), Datas_hyper[i], cdf_hyper[:, i, 1:]), dtype=np.int32))  # Cdf[Datas + 1 - Min_V]

            ans_enc = rans.RansEncoder()
            if min_hyper_2!=max_hyper_2:
                ans_enc.encode_batch(Cdf_0,Cdf_1)
            if min_hyper_1!=max_hyper_1:
                ans_enc.encode_batch(Cdf_lower_hyper1,Cdf_upper_hyper1)
            if len(Effective_Datas)>0:
                if min_main!=max_main:
                    ans_enc.encode_batch(Cdf_lower_main,Cdf_upper_main)
            bin_all = ans_enc.flush()
            file_size_main = len(bin_all)


        else:
            # Hyper Arith Encode
            min_hyper_1 = torch.min(y_hyper_q).cpu().numpy().astype(np.int)
            max_hyper_1 = torch.max(y_hyper_q).cpu().numpy().astype(np.int)
            _, c, h, w = y_hyper_q.shape
            # print("Hyper Channel:", c)
            Datas_hyper = torch.reshape(
                y_hyper_q, [c, -1]).cpu().numpy().astype(np.int)
            # [Min_V - 0.5 , Max_V + 0.5]
            # sample = np.arange(min_hyper_1, max_hyper_1 + 1 + 1)
            # sample = np.tile(sample, [c, 1, 1])
            # sample_tensor = torch.FloatTensor(sample)
            # if header.GPU:
            #     sample_tensor = sample_tensor.to(device)
            # lower = torch.sigmoid(self.factorized_entropy_func._logits_cumulative(
            #     sample_tensor - 0.5, stop_gradient=False))
            # cdf_h = lower.data.cpu().numpy() * ((1 << precise) - (max_hyper_1 -
            #                                                     min_hyper_1 + 1))  # [N1, 1, Max-Min+1]
            # cdf_h = cdf_h.astype(np.int) + sample.astype(np.int) - min_hyper_1
            cdf_h = self.factorized_entropy_func.make_cdf(min_hyper_1, max_hyper_1)
            cdf_hyper = np.reshape(np.tile(cdf_h, [len(Datas_hyper[0]), 1, 1, 1]), [
                len(Datas_hyper[0]), c, -1])

            # Datas_hyper [256 N], cdf_hyper [256,1,X]
            Cdf_0, Cdf_1 = [], []
            for i in range(c):
                Cdf_0.extend(np.fromiter(map(lambda x, y: int(
                    y[x - min_hyper_1]), Datas_hyper[i], cdf_hyper[:, i, :]), dtype=np.int32))  # Cdf[Datas - Min_V]
                Cdf_1.extend(np.fromiter(map(lambda x, y: int(
                    y[x - min_hyper_1]), Datas_hyper[i], cdf_hyper[:, i, 1:]), dtype=np.int32))  # Cdf[Datas + 1 - Min_V]
                
            ans_enc = rans.RansEncoder()
            if min_hyper_1!=max_hyper_1:
                ans_enc.encode_batch(Cdf_0,Cdf_1)
            if min_main!=max_main:
                ans_enc.encode_batch(Cdf_lower_main,Cdf_upper_main)
            bin_all = ans_enc.flush()
            file_size_main = len(bin_all)

        slice_header = picture.slice.slice_header
        slice_header.min_main, slice_header.max_main = min_main, max_main
        slice_header.min_hyper_1, slice_header.max_hyper_1 = min_hyper_1, max_hyper_1
        slice_header.min_hyper_2, slice_header.max_hyper_2 = min_hyper_2, max_hyper_2
        slice_header.file_size_main = file_size_main
        print("=====", file_size_main)
        slice_header.threshold = threshold
        # slice_header.file_size_main, slice_header.file_size_hyper_1, slice_header.file_size_hyper_2 = file_size_main, file_size_hyper_1, file_size_hyper_2
        file_object = slice_header.write_header_to_stream(file_object) # write slice header
        file_object.write(bin_all)
        return file_object