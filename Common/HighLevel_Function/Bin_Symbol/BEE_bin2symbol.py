import torch
import torch.nn as nn
import torch.nn.functional as F
import einops
from Common.models.BEE_models.quantmodule import quantHSS
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockUpsample,
    subpel_conv3x3,
    MaskedConv2d,
    conv,
    deconv,
)

MeanAndResidualScale = True

from Common.models.BEE_models.ssc import CSSC


class BEE_Bin2Symbol(nn.Module):
    def __init__(self, N=192, M=192, refine=4, init_weights=True, Quantized=False, oldversion=False, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.N = int(N)
        self.M = int(M)
        self.Quantized = Quantized
        self.filterCoeffs1 = []
        self.filterCoeffs2 = []
        self.filterCoeffs3 = []
        self.numfilters = [0, 0, 0]
        self.numthreads_min = 50
        self.numthreads_max = 100
        self.waveShift = 1
        self.kernel_height = 3
        self.kernel_width = 4
        self.DeterminismSpeedup = True
        self.DoublePrecisionProcessing = False
        self.channelOffsetsTool = False
        self.decChannelOffsets = []
        self.offsetSplit_w = 0
        self.offsetSplit_h = 0

        self.cssc = CSSC(self.M)

        self.h_s = nn.Sequential(
            deconv(N, M, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            deconv(M, M * 3 // 2, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 3 // 2, M * 2, stride=1, kernel_size=3, device=device),
        )
        
        if self.Quantized:
            self.h_s_scale = quantHSS(self.N, self.M, vbit=16, cbit=16, use_float=False, device=device)
            self.sbit = 12
        else:
            self.h_s_scale = nn.Sequential(
                deconv(N, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                deconv(M, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
            )

    def get_mask(self, scales_hat, filtercoeffs):
        mode = filtercoeffs["mode"]
        thr = filtercoeffs["thr"]
        block_size = filtercoeffs["block_size"]
        greater = filtercoeffs["greater"]
        scale = filtercoeffs["scale"]
        channels = filtercoeffs["channels"]
        mask = None
        self.likely = torch.abs(scales_hat)

        _, _, h, w = scales_hat.shape
        h_pad = ((h + block_size - 1) // block_size) * block_size - h
        w_pad = ((w + block_size - 1) // block_size) * block_size - w
        likely = F.pad(self.likely, (0, w_pad, 0, h_pad), value=1)
        if mode == 1:  # minpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = -(maxpool(-likely))
        if mode == 2:  # avgpool
            avgpool = torch.nn.AvgPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = avgpool(likely)
        if mode == 3:  # maxpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = (maxpool(likely))

        if greater:
            mask = (likely > thr)
        else:
            mask = (likely < thr)
        if mode == 4:  # maxpool
            for i in range(192):
                if i in channels:
                    pass
                else:
                    mask[:, i, :, :] = False
        mask = einops.repeat(mask, 'a b c d -> a b (c repeat1) (d repeat2)', repeat1=block_size, repeat2=block_size)
        mask = F.pad(mask, (0, -w_pad, 0, -h_pad), value=1)
        return mask, scale

    def LSBS(self, w_hat, quant_latent, scales_hat):  # -> Tensor quant_latent
        if MeanAndResidualScale:
            means_full = quant_latent - w_hat
            self.decParameters = [quant_latent, w_hat, scales_hat]
            for i in range(self.numfilters[2]):
                mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs3[i]))
                intermediate = quant_latent + (scale[0] * means_full + scale[1] * w_hat)
                quant_latent = torch.where(mask, intermediate, quant_latent)
        return quant_latent

    def LDAO(self, quant_latent, height, width, device):  # -> Tensor qunat_latent
        if self.channelOffsetsTool:
            wpad = ((width + self.offsetSplit_w - 1) // self.offsetSplit_w) * self.offsetSplit_w - width
            hpad = ((height + self.offsetSplit_h - 1) // self.offsetSplit_h) * self.offsetSplit_h - height
            kernel_w = (width + wpad) // self.offsetSplit_w
            kernel_h = (height + hpad) // self.offsetSplit_h
            self.decChannelOffsets = self.decChannelOffsets.to(device).unsqueeze(0)
            self.decChannelOffsets = einops.repeat(self.decChannelOffsets, 'a b c d -> a b (c repeat1) (d repeat2)',
                                                   repeat1=kernel_h, repeat2=kernel_w)
            self.decChannelOffsets = F.pad(self.decChannelOffsets, (0, -wpad, 0, -hpad))
            quant_latent += self.decChannelOffsets
        return quant_latent

    def HyperDecoding(self, z_hat):  # ->FeatureMap1
        FeatureMap1 = self.h_s(z_hat)
        return FeatureMap1


    def Decouple_net(self, w_hat, params):
        if w_hat.is_cuda:
            self.cssc.half()
            w_hat = w_hat.half()
            params = params.half()
            y_hat, means_hat = self.cssc.decode(w_hat, params)
            y_hat = y_hat.to(torch.float32)
            means_hat = means_hat.to(torch.float32)
        else:
            y_hat, means_hat = self.cssc.decode(w_hat, params)
        
        return y_hat

    def update(self, picture):
        self.numfilters = [picture.picture_header.coding_tool.mask_scale.num_adaptive_quant_params,
                           picture.picture_header.coding_tool.mask_scale.num_block_based_skip_params,
                           picture.picture_header.coding_tool.mask_scale.num_latent_post_process_params]
        if self.numfilters[0]:
            self.filterCoeffs1 = picture.picture_header.coding_tool.mask_scale.filterList[0:self.numfilters[0]]
        if self.numfilters[1]:
            self.filterCoeffs2 = picture.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0]:self.numfilters[0] + self.numfilters[1]]
        if self.numfilters[2]:
            self.filterCoeffs3 = picture.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0] + self.numfilters[1]:]
        self.DeterminismSpeedup = picture.picture_header.coding_tool.deterministic_processing_flag
        self.DoublePrecisionProcessing = picture.picture_header.coding_tool.double_precision_processing_flag
        self.channelOffsetsTool = picture.picture_header.coding_tool.adaptive_offset.adaptive_offset_enabled_flag
        self.decChannelOffsets = picture.picture_header.coding_tool.adaptive_offset.offsetList
        self.offsetSplit_w = picture.picture_header.coding_tool.adaptive_offset.num_horizontal_split
        self.offsetSplit_h = picture.picture_header.coding_tool.adaptive_offset.num_vertical_split

    def decode(self, varlist, device):
        z_hat = varlist[0]
        w_hat = varlist[1]
        scales_hat = varlist[2]

        s = 4
        kernel_size = 5
        padding = (kernel_size - 1) // 2
        height = z_hat.size(2) * s
        width = z_hat.size(3) * s

        featureMap1 = self.HyperDecoding(z_hat)
        quant_latent = self.Decouple_net(w_hat, featureMap1)
        quant_latent = self.LDAO(quant_latent, height, width, device)
        quant_latent = self.LSBS(w_hat, quant_latent, scales_hat)
        return quant_latent
