import struct
import time
import torch
import torch.nn as nn
import numpy as np
from Common.models.NIC_models.basic_module import P_NN
from Common.models.NIC_models.factorized_entropy_model import Entropy_bottleneck
from Common.models.NIC_models.hyper_module import h_synthesisTransform
from Common.models.NIC_models.functional_class import NormalCDF
from Common.models.NIC_models.context_model import Weighted_Gaussian, Weighted_Gaussian_res_2mask, P_Model
from Common.utils.nic_utils.config import dict
import rans

from Common.models.NIC_models.quantization import *
from Common.models.NIC_models.quantization.quant_registry import nic_quantize_class

@nic_quantize_class
class NIC_Bits2Bin(nn.Module):
    def __init__(self, picture, **kwargs):
        super().__init__()
        # device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        header = picture.picture_header
        H, W, model_index = header.picture_size_h, header.picture_size_w, header.parameter_set_id
        C = 3

        # H_offset = 0
        # W_offset = 0
        header.slice_height = int(2 ** (header.log2_slice_size_h_minus6 + 6))
        header.slice_width = int(2 ** (header.log2_slice_size_w_minus6 + 6))
        header.Slice_Num_in_Width = int(np.ceil(W / header.slice_width))
        header.Slice_Num_in_Height = int(np.ceil(H / header.slice_height))

        M, N2 = 192, 128
        header.USE_VR_MODEL = 0 # default set as false
        
        if header.model_id == 0 or header.model_id == 1:
            header.USE_VR_MODEL = 1
            lambda_rd_nom_used = header.quality_id / pow(2, 16) * 1.2
            # print(lambda_rd_nom_used)
            lambda_rd_numpy = np.zeros((1, 1), np.float32)
            lambda_rd_numpy[0, 0] = lambda_rd_nom_used
            header.lambda_rd = torch.Tensor(lambda_rd_numpy)    
            
            if (model_index in [1,2]) or (model_index in [4,5]): # VR_High
                M, N2 = 256, 192
                
            if header.coding_tool.multi_hyper_flag:
                if header.coding_tool.predictor_flag:
                    # self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
                    self.Y_2 = P_NN(256, 2)
                else:
                    # self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
            else:
                # self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(N2)
        else:
            if header.coding_tool.multi_hyper_flag:
                if header.coding_tool.predictor_flag:
                    # self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
                    self.Y_2 = P_NN(256, 2)
                else:
                    # self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
            else:
                # self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(N2)
            header.lambda_rd = None

        self.c_main = M
        if header.coding_tool.multi_hyper_flag:
            self.c_hyper = 256
            self.c_hyper_2 = 128
        else:
            self.c_hyper = N2
        header.M, header.N2 = M, N2
        self.normal_cdf = NormalCDF()
        
    def update(self,header):
        raise NotImplementedError

    def decode(self, header, slice_header, dec, device):
        precise = 16

        if header.coding_tool.multi_hyper_flag:
            ############### Hyper 2 Decoder ###############
            cdf_h = self.factorized_entropy_func.make_cdf(slice_header.min_hyper_2, slice_header.max_hyper_2)

            print(slice_header.min_hyper_2, slice_header.max_hyper_2)
            Recons = []
            if slice_header.min_hyper_2==slice_header.max_hyper_2:
                count = self.c_hyper_2 * int(slice_header.enc_height * slice_header.enc_width / 64 / 64)
                Recons.extend([slice_header.min_hyper_2] * count)
            else:
                count = int(slice_header.enc_height * slice_header.enc_width / 64 / 64)
                # print("==============", cdf_h[:self.c_hyper_2, 0, :])
                # print("==============", cdf_h[:self.c_hyper_2, 0, :].shape)
                # print(count)
                Recons.extend([x + slice_header.min_hyper_2 for x in dec.decode_batch(cdf_h[:self.c_hyper_2, 0, :].repeat(count, axis=0))])
                
            # reshape Recons to y_hyper_q   [1, c_hyper, H_PAD/64, W_PAD/64]
            # correct H_PAD/W_PAD to enc_height/enc_width. by gzq
            y_hyper_2_q = torch.reshape(torch.Tensor(
                Recons), [1, self.c_hyper_2, int(slice_header.enc_height / 64), int(slice_header.enc_width / 64)])
            
            # print("[bit2bin] max (min) hyper 2: %d, %d" % (slice_header.max_hyper_2, slice_header.min_hyper_2))

            #IPython.embed()
            ############### Hyper 1 Decoder ###############
            # hyper_dec = image_comp.p(image_comp.hyper_dec(y_hyper_q))
            y_hyper_2_q = y_hyper_2_q.to(device)
                    
            tmp2 = self.hyper_2_dec(y_hyper_2_q)
            hyper_2_dec = self.p_2(tmp2)
            # print("hyper_2_dec",hyper_2_dec.mean())
            _, c, h, w = hyper_2_dec.shape
            c //= 2
            mean = hyper_2_dec[:, :c, :, :].unsqueeze(-1)
            scale = hyper_2_dec[:, c:, :, :].unsqueeze(-1)
            scale = torch.abs(scale)
            scale[scale < 1e-6] = 1e-6
            #import IPython
            #IPython.embed()

            sample = np.arange(slice_header.min_hyper_1, slice_header.max_hyper_1+1+1)  # [Min_V - 0.5 , Max_V + 0.5]
            sample = torch.FloatTensor(np.tile(sample, [1, c, h, w, 1])).to(device)
            # sample = torch.FloatTensor(np.tile(sample, [1, c, h, w, 1]))
            
            lower = torch.zeros(1, c, h, w, slice_header.max_hyper_1-slice_header.min_hyper_1+2).to(device)
            lower = self.normal_cdf(sample - 0.5, scale, mean)

            cdf_m = lower.data.cpu().numpy()*((1 << precise) - (slice_header.max_hyper_1 - slice_header.min_hyper_1 + 1))
            cdf_m = cdf_m.astype(np.int32) + sample.cpu().numpy().astype(np.int32) - slice_header.min_hyper_1

            Recons = []
            if slice_header.min_hyper_1==slice_header.max_hyper_1:
                count = c * int(h) * int(w)
                Recons.extend([slice_header.min_hyper_1] * count)
            else:
                Recons.extend([x + slice_header.min_hyper_1 for x in dec.decode_batch(cdf_m[0, :c, :int(h), :int(w), :].reshape(c*int(h)*int(w), -1))])

            y_hyper_q = torch.reshape(torch.Tensor(Recons), [1, c, h, w])
            y_hyper_q = y_hyper_q.to(device)
            # for i in range(y_hyper_q.shape[1]):
            #     if i == 241:
            #         print("[bin2bits] y_hyper_q", i, y_hyper_q[0,i,:, :])
            if header.coding_tool.predictor_flag:
                y_hyper_q = y_hyper_q + self.Y_2(tmp2)

        else: # Single Hyper Model
            ############### Hyper Decoder ###############
            cdf_h = self.factorized_entropy_func.make_cdf(slice_header.min_hyper_1, slice_header.max_hyper_1)
            Recons = []
            if slice_header.min_hyper_1==slice_header.max_hyper_1:
                count = self.c_hyper * int(slice_header.enc_height * slice_header.enc_width / 64 / 64)
                Recons.extend([slice_header.min_hyper_1] * count)
            else:
                count = int(slice_header.enc_height * slice_header.enc_width / 64 / 64)
                Recons.extend([x + slice_header.min_hyper_1 for x in dec.decode_batch(cdf_h[:self.c_hyper, 0, :].repeat(count, axis=0))])
            # reshape Recons to y_hyper_q   [1, c_hyper, H_PAD/64, W_PAD/64]
            y_hyper_q = torch.reshape(torch.Tensor(
                Recons), [1, self.c_hyper, int(slice_header.enc_height / 64), int(slice_header.enc_width / 64)])
            
            y_hyper_q = y_hyper_q.to(device)
                
            print("[bits2bin] check final")
        slice_header.c_main = self.c_main
        # print("[bits2bin] y_hyper_q", torch.max(y_hyper_q))
        return y_hyper_q, slice_header#, bin_dir_path