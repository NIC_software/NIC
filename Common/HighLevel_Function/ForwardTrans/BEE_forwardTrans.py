import torch
import torch.nn as nn
import torch.nn.functional as F
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockWithStride,
    conv3x3,
    conv,
)
from Common.models.NIC_models.basic_module import Non_local_Block, ResBlock


class Enc(nn.Module):
    def __init__(self, num_features, N1, N2, M, M1):
        #input_features = 3, N1 = 192, N2 = 128, M = 192, M1 = 96
        super(Enc, self).__init__()
        self.N1 = int(N1)
        self.N2 = int(N2)
        self.M = int(M)
        self.M1 = int(M1)
        self.n_features = int(num_features)

        self.conv1 = nn.Conv2d(self.n_features, self.M1, 5, 1, 2)
        self.trunk1 = nn.Sequential(ResBlock(self.M1, self.M1, 3, 1, 1), ResBlock(
            self.M1, self.M1, 3, 1, 1), nn.Conv2d(self.M1, 2*self.M1, 5, 2, 2))

        self.down1 = nn.Conv2d(2*self.M1, self.M, 5, 2, 2)
        self.trunk2 = nn.Sequential(ResBlock(2*self.M1, 2*self.M1, 3, 1, 1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                    ResBlock(2*self.M1, 2*self.M1, 3, 1, 1))
        self.mask1 = nn.Sequential(Non_local_Block(2*self.M1, self.M1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   ResBlock(
                                       2*self.M1, 2*self.M1, 3, 1, 1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   nn.Conv2d(2*self.M1, 2*self.M1, 1, 1, 0))

        self.trunk3 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 5, 2, 2))


        self.trunk4 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 5, 2, 2))

        self.trunk5 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1))
        self.mask2 = nn.Sequential(Non_local_Block(self.M, self.M // 2), ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 1, 1, 0))

    def forward(self, x, lambda_rd=None):

        x1 = self.conv1(x)
        x2 = self.trunk1(x1)
        x3 = self.trunk2(x2) + x2
        x3 = self.down1(x3)
        x4 = self.trunk3(x3)
        x5 = self.trunk4(x4)
        x6 = self.trunk5(x5) * F.sigmoid(self.mask2(x5)) + x5
        return x6


class BEE_ForwardTrans(nn.Module):
    def __init__(self, N=192, M=192, use_nic_trans=False, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") is None else kwargs.get("device")
        self.DeterminismSpeedup = True
        # self.encParams = None
        # self.encSkip = False
        self.N1 = int(192)
        self.N2 = int(128)
        self.M0 = int(192)                            # self.M = int(192)
        self.M1 = int(96)
        self.input_features = int(3)    
        self.encoder = Enc(self.input_features, self.N1, self.N2, self.M0, self.M1)
        if use_nic_trans:
            nic_trans = Enc(3, 192, 128, 192, 96)
            self.encoder = nic_trans

        self.h_a = nn.Sequential(
            conv(M, N, stride=1, kernel_size=3),
            nn.LeakyReLU(inplace=True),
            conv(N, N, stride=2, kernel_size=5),
            nn.LeakyReLU(inplace=True),
            conv(N, N, stride=2, kernel_size=5),
        )

    @staticmethod
    def splitFunc(x, func, splitNum, pad, crop):
        _, _, _, w = x.shape
        w_step = ((w + splitNum - 1) // splitNum)
        pp = []
        for i in range(splitNum):
            start_offset = pad if i > 0 else 0
            start_crop = crop if i > 0 else 0
            end_offset = pad if i < (splitNum - 1) else 0
            end_crop = crop if i < (splitNum - 1) else 0
            dummy = func(x[:, :, :, (w_step * i) - start_offset:w_step * (i + 1) + end_offset])
            dummy = dummy[:, :, :, start_crop:]
            if end_crop > 0:
                dummy = dummy[:, :, :, :-end_crop]
            pp.append(dummy)
        x_hat = torch.cat(pp, dim=3)
        return x_hat

    def update(self, header):
        self.DeterminismSpeedup = header.picture_header.coding_tool.deterministic_processing_flag

    def encode(self, input_variable):
        imArray = input_variable[0]
        x_pad = input_variable[1]
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True
        y = self.splitFunc(x_pad, self.encoder, 4, 64, 4)
        z = self.h_a(y)
        return [y, z, input_variable[0], input_variable[1]]
