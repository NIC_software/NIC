import torch.nn as nn

class BaseForwardTrans(nn.Module):
    def __init__(self):
        super().__init__()
        
    def decode(self, x, header):
        # return quantized latent y_hat (example: y_hat)
        raise NotImplementedError
        