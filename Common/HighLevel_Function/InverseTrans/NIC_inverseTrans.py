import torch.nn as nn
import torch
import torch.nn.functional as F

from Common.models.NIC_models.model import Dec


class NIC_InverseTrans(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        input_features = 3
        M = header.M
        N1 = M
        M1 = M // 2
        self.decoder = Dec(input_features, N1, M, M1, header.USE_VR_MODEL)

    def decode(self, quant_latent, header, header_block):
        # imArray = self.SynTrans(quant_latent)
        rec = self.decoder(quant_latent, header.lambda_rd)

        ############################ Reverse Geometric Flip and Rotate ########################
        geo_index = header_block.geo_index
        # print(geo_index, header.geo_flag)
        if header.coding_tool.geo_flag:
            i_rot = int(geo_index % 4)
            if geo_index < 4:
                rec = torch.rot90(rec, k=4 - i_rot, dims=[2, 3])
            else:
                rec = torch.flip(torch.rot90(rec, k=4 - i_rot, dims=[2, 3]), dims=[2])

        output_ = torch.clamp(rec, min=0., max=1.0)
        out = output_.data[0].cpu().numpy()
        out = out.transpose(1, 2, 0)
        return out
