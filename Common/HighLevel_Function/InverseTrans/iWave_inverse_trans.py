import torch
import torch.nn as nn
import numpy as np

from PIL import Image
from torch.autograd import Variable
from torch.nn import functional as F

from Common.utils.iWave_utils.utils import (find_min_and_max, img2patch, img2patch_padding, yuv2rgb_lossless,
                                            patch2img, rgb2yuv, yuv2rgb, dec_binary, model_forward)

from Common.utils.iWave_utils.img_resize import imresize
import rans
import struct

def read_rv_len(byte_data, n):
    fmt = ">{:d}I".format(n)
    expected_size = struct.calcsize(fmt)
    return list(struct.unpack(fmt, byte_data[:expected_size]))

def concat(blocks_rec):
    value = torch.cat([torch.cat(blocks_h, dim=3) for blocks_h in blocks_rec], dim=2)
    return value

PRECISION = 16 
FACTOR = torch.tensor(2, dtype=torch.float32).pow_(PRECISION)
def _convert_to_int_and_normalize(cdf_float, needs_normalization=True):
    Lp = cdf_float.shape[-1]
    factor = torch.tensor(2, dtype=torch.float32, device=cdf_float.device).pow_(PRECISION)
    new_max_value = factor
    if needs_normalization:
        new_max_value = new_max_value - (Lp - 1)
    cdf_float = cdf_float.mul(new_max_value)
    cdf_float = cdf_float.round()
    cdf = cdf_float.to(dtype=torch.int32, non_blocking=True)
    if needs_normalization:
        r = torch.arange(Lp, dtype=torch.int32, device=cdf.device)
        cdf.add_(r)
    return cdf

class iWave_InverseTrans(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        self.header = header
        self.coding_tool = header.coding_tool
        self.models_dict = None

    def decode(self, header, recon_path, string):

        if header.model_id == 2:
            recon = self.dec_lossless(header, recon_path, string)
        else:
            recon = self.dec_lossy(header, string)

        return recon

    def dec_lossless(self, header, recon_path, string):
        # read block pre-data
        len_num = int(read_rv_len(string[:struct.calcsize(">I")], 1)[0])
        rv_len_list = read_rv_len(string[struct.calcsize(">I"):], len_num)
        rv_tmp_len = int(read_rv_len(string[struct.calcsize(">I") + struct.calcsize(">I") * len_num:], 1)[0])
        
        string = string[struct.calcsize(">I") + struct.calcsize(">I") * len_num + struct.calcsize(">I"):]
        
        ans_dec = rans.RansDecoder()
        ans_dec.init(string,0,1000)

        trans_steps = 4
        code_block_size = self.coding_tool.code_block_size
        split_block_height = 2**(self.header.log2_slice_size_h_minus6 + 6)
        split_block_width = 2**(self.header.log2_slice_size_w_minus6 + 6)

        height_all = header.picture_size_h
        width_all = header.picture_size_w

        with torch.no_grad():
            ori_img = torch.zeros(1, 3, height_all, width_all)
            blocks_h = list(torch.split(ori_img, split_block_height, dim=2))
            blocks = [list(torch.split(block_h, split_block_width, dim=3)) for block_h in blocks_h]
            num_blocks = len(blocks)*len(blocks[0])
            print(f'Split the img into blocks, block size is {split_block_height} * {split_block_width}, num_blocks is {num_blocks}')
            
            blocks_rec = []
            block_idx = 0
            min_v = np.zeros(shape=(3, 13), dtype=np.int)
            max_v = np.zeros(shape=(3, 13), dtype=np.int)
            for i in range(3):
                for j in range(13):
                    min_v[i, j] = dec_binary(ans_dec, 15) - 6016
                    max_v[i, j] = dec_binary(ans_dec, 15) - 6016

            yuv_low_bound = min_v.min(axis=0)
            yuv_high_bound = max_v.max(axis=0)
            shift_min = min_v - yuv_low_bound
            shift_max = max_v - yuv_low_bound
            
            rv_list = []
            begin_index = 0
            for i in range(len_num):
                if i == 0:
                    begin_index = rv_tmp_len
                else:
                    begin_index = end_index
                end_index = begin_index + rv_len_list[i]
                rv_list.append(string[begin_index: end_index])
                
            cnt = 0    
            for b_h in blocks:
                blocks_h_rec = []
                for block in b_h:
                    string = rv_list[cnt]
                    cnt += 1

                    ans_dec = rans.RansDecoder()
                    ans_dec.init(string,0,1000)
                    
                    print(f'The {block_idx} block is being decoded')
                    img = block

                    size = img.size()
                    height = size[2]
                    width = size[3]

                    pad_h = int(np.ceil(height / 32)) * 32 - height
                    pad_w = int(np.ceil(width / 32)) * 32 - width

                    if (height + pad_h) // 16 < 3:
                        pad_h = pad_h + 32
                    if (width + pad_w) // 16 < 3:
                        pad_w = pad_w + 32

                    LL = torch.zeros(3, 1, (height + pad_h) // 16, (width + pad_w) // 16).cuda()
                    HL_list = []
                    LH_list = []
                    HH_list = []
                    down_scales = [2, 4, 8, 16]
                    for i in range(trans_steps):
                        HL_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        LH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        HH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())

                    subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
                    subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]
                    padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
                    padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

                    # used_scale = models_dict['transform'].used_scale()

                    coded_coe_num = 0
                    # decompress LL
                    paddings = (6, 6, 6, 6)
                    enc_LL = F.pad(LL, paddings, "constant")

                    _, _, H, W = LL.shape
                    temp_add_tensor = 1
                    for h_i in range(H):
                        for w_i in range(W):
                            cur_ct = enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13]

                            prob = self.models_dict['entropy_LL'](cur_ct, yuv_low_bound[0], yuv_high_bound[0]+1)
                            prob = prob.to(torch.float32)
                            prob = torch.clamp(prob, 0, 1-1e-6)
                            index = []

                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                Lp_list = []
                                for p in range(3):
                                    Lp_list.append(shift_max[p, 0] + 2 - shift_min[p, 0])
                                Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                factor = FACTOR.to(prob.device)
                                new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                for p in range(3):
                                    add_tensor[p] = add_tensor[p] - shift_min[p, 0]
                                if prob.shape[0] != 3:
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                            prob = prob.mul(new_max_value).round().add(add_tensor).to(dtype=torch.int32, non_blocking=True).tolist()

                            for sample_idx, prob_sample in enumerate(prob):
                                yuv_flag = sample_idx % 3
                                if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                    quantized_cdf = prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 2]
                                    dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, 0]
                                else:
                                    dec_c = min_v[yuv_flag, 0]
                                index.append(dec_c)
                            enc_LL[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                    LL = enc_LL[:, :, 6:-6, 6:-6]
                    print("LL decoded")
                    context_list = []
                    context_list.append(LL)

                    shuffle_factor = 2
                    pixel_shuffle = torch.nn.PixelShuffle(shuffle_factor)
                    pixel_unshuffle = torch.nn.PixelUnshuffle(shuffle_factor)
                    module_step = [4, 3, 2, 2]
                    # decompress HL, LH, HH
                    for i in range(trans_steps):
                        j = trans_steps - 1 - i

                        for idx in range(3):

                            if idx is 0:
                                module_name = 'entropy_HL'
                                module_num = 3 * j + 1
                                enc_oth = HL_list[j]
                            elif idx is 1:
                                module_name = 'entropy_LH'
                                module_num = 3 * j + 2
                                enc_oth = LH_list[j]
                            elif idx is 2:
                                module_name = 'entropy_HH'
                                module_num = 3 * j + 3
                                enc_oth = HH_list[j]

                            enc_oth = pixel_unshuffle(enc_oth)
                            enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")
                            context = torch.cat(context_list, dim=1)
                            context = F.pad(pixel_unshuffle(context), (6, 6, 6, 6), "constant")

                            if (module_step[j] == 4):
                                for encode_idx in range(4):

                                    if encode_idx is 0:
                                        context = context
                                        encode_num = 0
                                    elif encode_idx is 1:
                                        context = torch.cat([context, enc_oth_con[:, 0:1]], dim=1)
                                        encode_num = 3
                                    elif encode_idx is 2:
                                        context = torch.cat([context, enc_oth_con[:, 3:4]], dim=1)
                                        encode_num = 1
                                    elif encode_idx is 3:
                                        context = torch.cat([context, enc_oth_con[:, 1:2]], dim=1)
                                        encode_num = 2

                                    prob1 = model_forward(self.models_dict[module_name], context, yuv_low_bound,
                                                          yuv_high_bound, module_num, j, encode_idx)
                                    prob1 = prob1.to(torch.float32)
                                    prob1 = torch.clamp(prob1, 0, 1-1e-6)
                                    prob_size = prob1.size()
                                    prob1 = prob1.view(-1, prob1.size()[4])
                                    index = []

                                    if encode_idx == 0:
                                        Lp_list = []
                                        for p in range(3):
                                            Lp_list.append(shift_max[p, module_num] + 2 - shift_min[p, module_num])
                                        Lp_tensor = torch.tensor(Lp_list).unsqueeze(1).repeat(1,
                                                                                              prob1.shape[0] // 3).view(
                                            -1)
                                        factor = FACTOR.to(prob1.device)
                                        new_max_value = (factor + 1 - Lp_tensor.to(prob1.device)).unsqueeze(1)
                                        add_tensor = torch.arange(prob1.shape[-1], dtype=torch.int32,
                                                                  device=prob1.device).repeat(3, 1)
                                        for p in range(3):
                                            add_tensor[p] = add_tensor[p] - shift_min[p, module_num]
                                        add_tensor = add_tensor.unsqueeze(1).repeat(1, prob1.shape[0] // 3, 1).view(-1,
                                                                                                                    add_tensor.size()[
                                                                                                                        -1])
                                    prob1 = prob1.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                        'int32').tolist()

                                    for sample_idx, prob_sample in enumerate(prob1):
                                        # yuv_flag = sample_idx % 3
                                        yuv_flag = sample_idx // (len(prob1) // 3)
                                        # if True:
                                        if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                            # datas = index[sample_idx] - min_v[yuv_flag, module_num]
                                            # assert datas >= 0
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                            yuv_flag, module_num] + 2]
                                            dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                        else:
                                            dec_c = min_v[yuv_flag, module_num]
                                        index.append(dec_c)
                                    enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                        np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                    prob_size[2], prob_size[3])
                                    enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")
                            if (module_step[j] == 3):
                                for encode_idx in range(3):
                                    if encode_idx is 0:
                                        context = context
                                        encode_num = 0
                                        prob1 = model_forward(self.models_dict[module_name], context, yuv_low_bound,
                                                              yuv_high_bound, module_num, j, encode_idx)
                                        prob1 = prob1.to(torch.float32)
                                        prob1 = torch.clamp(prob1, 0, 1-1e-6)
                                        prob_size = prob1.size()
                                        prob1 = prob1.view(-1, prob1.size()[4])
                                        index = []
                                        Lp_list = []
                                        for p in range(3):
                                            Lp_list.append(shift_max[p, module_num] + 2 - shift_min[p, module_num])
                                        Lp_tensor = torch.tensor(Lp_list).unsqueeze(1).repeat(1,
                                                                                              prob1.shape[0] // 3).view(
                                            -1)
                                        factor = FACTOR.to(prob1.device)
                                        new_max_value = (factor + 1 - Lp_tensor.to(prob1.device)).unsqueeze(1)
                                        add_tensor = torch.arange(prob1.shape[-1], dtype=torch.int32,
                                                                  device=prob1.device).repeat(3, 1)
                                        for p in range(3):
                                            add_tensor[p] = add_tensor[p] - shift_min[p, module_num]
                                        add_tensor = add_tensor.unsqueeze(1).repeat(1, prob1.shape[0] // 3, 1).view(-1,
                                                                                                                    add_tensor.size()[
                                                                                                                        -1])
                                        prob1 = prob1.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()

                                        for sample_idx, prob_sample in enumerate(prob1):
                                            yuv_flag = sample_idx // (len(prob1) // 3)
                                            # if True:
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                # datas = index[sample_idx] - min_v[yuv_flag, module_num]
                                                # assert datas >= 0
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")
                                    elif encode_idx is 1:
                                        context = torch.cat([context, enc_oth_con[:, 0:1]], dim=1)

                                        encode_num = 1
                                        prob1 = model_forward(self.models_dict[module_name], context, yuv_low_bound,
                                                              yuv_high_bound, module_num, j, encode_idx)
                                        prob1 = prob1.to(torch.float32)
                                        prob1 = torch.clamp(prob1, 0, 1-1e-6)
                                        prob_size = prob1.size()
                                        prob1 = prob1.view(-1, prob1.size()[4])
                                        prob1 = prob1.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()
                                        index = []
                                        for sample_idx, prob_sample in enumerate(prob1):
                                            yuv_flag = sample_idx // (len(prob1) // 3)
                                            # if True:
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                # datas = index[sample_idx] - min_v[yuv_flag, module_num]
                                                # assert datas >= 0
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")
                                    elif encode_idx is 2:
                                        context = torch.cat([context, enc_oth_con[:, 1:2]], dim=1)
                                        prob1, prob2 = model_forward(self.models_dict[module_name], context,
                                                                     yuv_low_bound, yuv_high_bound, module_num, j,
                                                                     encode_idx, 1)
                                        prob1 = prob1.to(torch.float32)
                                        prob1 = torch.clamp(prob1, 0, 1-1e-6)
                                        prob2 = prob2.to(torch.float32)
                                        prob2 = torch.clamp(prob2, 0, 1-1e-6)
                                        
                                        prob_size = prob1.size()
                                        prob1 = prob1.view(-1, prob1.size()[4])
                                        prob1 = prob1.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()
                                        prob2 = prob2.view(-1, prob2.size()[4])
                                        prob2 = prob2.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()
                                        index = []
                                        encode_num = 2
                                        for sample_idx, prob_sample in enumerate(prob1):
                                            yuv_flag = sample_idx // (len(prob1) // 3)
                                            # if True:
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                # datas = index[sample_idx] - min_v[yuv_flag, module_num]
                                                # assert datas >= 0
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")

                                        index = []
                                        encode_num = 3
                                        for sample_idx, prob_sample in enumerate(prob2):
                                            yuv_flag = sample_idx // (len(prob2) // 3)
                                            # if True:
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                # datas = index[sample_idx] - min_v[yuv_flag, module_num]
                                                # assert datas >= 0
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")

                            if (module_step[j] == 2):
                                for encode_idx in range(2):
                                    if encode_idx is 0:
                                        context = context
                                        prob1, prob2 = model_forward(self.models_dict[module_name], context,
                                                                     yuv_low_bound, yuv_high_bound, module_num, j,
                                                                     encode_idx, 1)
                                        prob1 = prob1.to(torch.float32)
                                        prob1 = torch.clamp(prob1, 0, 1-1e-6)
                                        prob2 = prob2.to(torch.float32)
                                        prob2 = torch.clamp(prob2, 0, 1-1e-6)
                                        prob_size = prob1.size()

                                        prob1 = prob1.view(-1, prob1.size()[4])

                                        Lp_list = []
                                        for p in range(3):
                                            Lp_list.append(shift_max[p, module_num] + 2 - shift_min[p, module_num])

                                        Lp_tensor = torch.tensor(Lp_list).unsqueeze(1).repeat(1,
                                                                                              prob1.shape[0] // 3).view(
                                            -1)

                                        # Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                        factor = FACTOR.to(prob1.device)
                                        new_max_value = (factor + 1 - Lp_tensor.to(prob1.device)).unsqueeze(1)
                                        add_tensor = torch.arange(prob1.shape[-1], dtype=torch.int32,
                                                                  device=prob1.device).repeat(3, 1)
                                        for p in range(3):
                                            add_tensor[p] = add_tensor[p] - shift_min[p, module_num]
                                        add_tensor = add_tensor.unsqueeze(1).repeat(1, prob1.shape[0] // 3, 1).view(-1,
                                                                                                                    add_tensor.size()[
                                                                                                                        -1])

                                        prob1 = prob1.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()
                                        index = []
                                        encode_num = 0
                                        for sample_idx, prob_sample in enumerate(prob1):
                                            yuv_flag = sample_idx // (len(prob1) // 3)
                                            # if True:
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        # enc_oth_con = F.pad(enc_oth, (6,6,6,6), "constant")

                                        index = []
                                        encode_num = 3

                                        prob2 = prob2.view(-1, prob2.size()[4])
                                        prob2 = prob2.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()
                                        for sample_idx, prob_sample in enumerate(prob2):
                                            yuv_flag = sample_idx // (len(prob2) // 3)
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")
                                    elif encode_idx is 1:
                                        context = torch.cat([context, enc_oth_con[:, 0:1], enc_oth_con[:, 3:4]], dim=1)
                                        prob1, prob2 = model_forward(self.models_dict[module_name], context,
                                                                     yuv_low_bound, yuv_high_bound, module_num, j,
                                                                     encode_idx, 1)
                                        prob1 = prob1.to(torch.float32)
                                        prob1 = torch.clamp(prob1, 0, 1-1e-6)
                                        prob2 = prob2.to(torch.float32)
                                        prob2 = torch.clamp(prob2, 0, 1-1e-6)
                                        prob_size = prob1.size()
                                        prob1 = prob1.view(-1, prob1.size()[4])
                                        prob1 = prob1.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()
                                        prob2 = prob2.view(-1, prob2.size()[4])
                                        prob2 = prob2.mul(new_max_value).round().add(add_tensor).cpu().numpy().astype(
                                            'int32').tolist()
                                        index = []
                                        encode_num = 1
                                        for sample_idx, prob_sample in enumerate(prob1):
                                            yuv_flag = sample_idx // (len(prob1) // 3)
                                            # if True:
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")

                                        index = []
                                        encode_num = 2
                                        for sample_idx, prob_sample in enumerate(prob2):
                                            yuv_flag = sample_idx // (len(prob2) // 3)
                                            # if True:
                                            if shift_min[yuv_flag, module_num] < shift_max[yuv_flag, module_num]:
                                                quantized_cdf = prob_sample[shift_min[yuv_flag, module_num]:shift_max[
                                                                                                                yuv_flag, module_num] + 2]
                                                dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, module_num]
                                            else:
                                                dec_c = min_v[yuv_flag, module_num]
                                            index.append(dec_c)
                                        enc_oth[:, encode_num:encode_num + 1] = torch.from_numpy(
                                            np.array(index).astype(np.int)).cuda().view(prob_size[0], prob_size[1],
                                                                                        prob_size[2], prob_size[3])
                                        enc_oth_con = F.pad(enc_oth, (6, 6, 6, 6), "constant")

                            enc_oth = pixel_shuffle(enc_oth)

                            if idx is 0:
                                HL_list[j] = enc_oth[:, :, 0:subband_h[j], 0:subband_w[j]]
                                context_list.append(HL_list[j])
                                print("HL done")
                            elif idx is 1:
                                LH_list[j] = enc_oth[:, :, 0:subband_h[j], 0:subband_w[j]]
                                context_list.append(LH_list[j])
                                print("LH done")
                            elif idx is 2:
                                HH_list[j] = enc_oth[:, :, 0:subband_h[j], 0:subband_w[j]]
                                context_list.append(HH_list[j])
                                print("HH done")

                                if i < trans_steps - 1:
                                    for k, context_feature in enumerate(context_list):
                                        context_list[k] = imresize(context_feature, scale=2)

                                LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j],
                                                                                 j)

                    recon = LL
                    recon = recon.permute(1, 0, 2, 3)
                    recon = recon[:, :, 0:height, 0:width]

                    blocks_h_rec.append(recon)
                    print(f'The {block_idx} block finished')
                    block_idx += 1

                blocks_rec.append(blocks_h_rec)

            recon = concat(blocks_rec)
            recon = recon[0, :, :, :]
            recon = recon.permute(1, 2, 0)
            recon = recon.cpu().data.numpy()
            recon = yuv2rgb_lossless(recon).astype(np.float32)
            recon = np.clip(recon, 0., 255.).astype(np.uint8)
            img = Image.fromarray(recon, 'RGB')
            img.save(recon_path)

            return img

    def dec_lossy(self, header, string):

        ans_dec = rans.RansDecoder()
        ans_dec.init(string,0,1000)

        trans_steps = 4
        code_block_size = self.coding_tool.code_block_size
        split_block_height = 2**(self.header.log2_slice_size_h_minus6 + 6)
        split_block_width = 2**(self.header.log2_slice_size_w_minus6 + 6)
        
        height_all = header.picture_size_h
        width_all = header.picture_size_w

        with torch.no_grad():
            ori_img = torch.zeros(1, 3, height_all, width_all)
            blocks_h = list(torch.split(ori_img, split_block_height, dim=2))
            blocks = [list(torch.split(block_h, split_block_width, dim=3)) for block_h in blocks_h]
            num_blocks = len(blocks)*len(blocks[0])
            print(f'Split the img into blocks, block size is {split_block_height} * {split_block_width}, num_blocks is {num_blocks}')
            
            blocks_rec = []
            block_idx = 0

            min_v = np.zeros(shape=(3, 13), dtype=np.int)
            max_v = np.zeros(shape=(3, 13), dtype=np.int)
            for i in range(3):
                for j in range(13):
                    min_v[i, j] = dec_binary(ans_dec, 15) - 6016
                    max_v[i, j] = dec_binary(ans_dec, 15) - 6016
            yuv_low_bound = min_v.min(axis=0)
            yuv_high_bound = max_v.max(axis=0)
            shift_min = min_v - yuv_low_bound
            shift_max = max_v - yuv_low_bound

            for b_h in blocks:
                
                blocks_h_rec = []
                
                for block in b_h:
                    print(f'The {block_idx} block is being decoded')
                    height = block.size()[2]
                    width = block.size()[3]
                    
                    pad_h = int(np.ceil(height / 16)) * 16 - height
                    pad_w = int(np.ceil(width / 16)) * 16 - width

                    LL = torch.zeros(3, 1, (height + pad_h) // 16, (width + pad_w) // 16).cuda()
                    HL_list = []
                    LH_list = []
                    HH_list = []
                    down_scales = [2, 4, 8, 16]
                    for i in range(trans_steps):
                        HL_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        LH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        HH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())

                    subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
                    subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]
                    padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
                    padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

                    ll_scale, hl_scale, lh_scale, hh_scale = self.models_dict['transform'].used_scale()

                    coded_coe_num = 0
                    # decompress LL
                    tmp_stride = subband_w[3] + padding_sub_w[3]
                    tmp_hor_num = tmp_stride // code_block_size
                    paddings = (0, padding_sub_w[3], 0, padding_sub_h[3])
                    enc_LL = F.pad(LL, paddings, "constant")
                    enc_LL = img2patch(enc_LL, code_block_size, code_block_size, code_block_size)
                    paddings = (6, 6, 6, 6)
                    enc_LL = F.pad(enc_LL, paddings, "constant")

                    temp_add_tensor = 1
                    for h_i in range(code_block_size):
                        for w_i in range(code_block_size):
                            cur_ct = enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13]

                            prob = self.models_dict['entropy_LL'].inf_cdf_lower(cur_ct, yuv_low_bound[0], yuv_high_bound[0]+1)

                            index = []

                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                Lp_list = []
                                for p in range(3):
                                    Lp_list.append(shift_max[p, 0] + 2 - shift_min[p, 0])
                                Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                factor = FACTOR.to(prob.device)
                                new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                for p in range(3):
                                    add_tensor[p] = add_tensor[p] - shift_min[p, 0]
                                if prob.shape[0] != 3:
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                            prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                            for sample_idx, prob_sample in enumerate(prob):
                                coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                        h_i * tmp_stride + \
                                        ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                        w_i
                                if (coe_id % tmp_stride) < subband_w[3] and (coe_id // tmp_stride) < subband_h[3]:
                                    yuv_flag = sample_idx % 3
                                    if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                        quantized_cdf = prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 2]
                                        dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, 0]
                                    else:
                                        dec_c = min_v[yuv_flag, 0]
                                    coded_coe_num = coded_coe_num + 1
                                    index.append(dec_c)
                                else:
                                    index.append(0)
                            enc_LL[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                    LL = enc_LL[:, :, 6:-6, 6:-6]
                    LL = patch2img(LL, subband_h[3] + padding_sub_h[3], subband_w[3] + padding_sub_w[3])
                    LL = LL[:, :, 0:subband_h[3], 0:subband_w[3]]
                    print('LL decoded')

                    LL = LL * ll_scale
                    base_context = LL

                    for i in range(trans_steps):
                        j = trans_steps - 1 - i
                        x = 3 * j + 1
                        tmp_stride = subband_w[j] + padding_sub_w[j]
                        tmp_hor_num = tmp_stride // code_block_size
                        # compress HL
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HL_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        if padding_sub_w[j] >= base_context.shape[3] or padding_sub_h[j] >= base_context.shape[2]:
                            context = F.pad(base_context, paddings, "constant")
                        else:
                            context = F.pad(base_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HL'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)

                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        HL_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        HL_list[j] = patch2img(HL_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        HL_list[j] = HL_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('HL' + str(j) + ' decoded')

                        HL_list[j] = HL_list[j] * hl_scale

                        # compress LH
                        x = 3 * j + 2
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(LH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        temp_context = torch.cat((base_context, HL_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_LH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()
                                
                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        LH_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        LH_list[j] = patch2img(LH_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        LH_list[j] = LH_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('LH' + str(j) + ' decoded')

                        LH_list[j] = LH_list[j] * lh_scale

                        # compress HH
                        x = 3 * j + 3
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        temp_context = torch.cat((base_context, HL_list[j], LH_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                        
                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()
                                
                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = ans_dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        HH_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        HH_list[j] = patch2img(HH_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        HH_list[j] = HH_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('HH' + str(j) + ' decoded')

                        HH_list[j] = HH_list[j] * hh_scale

                        base_context = torch.cat((base_context, HL_list[j], LH_list[j], HH_list[j]), dim=1)
                        base_context = F.interpolate(base_context, scale_factor=2, mode='bicubic', align_corners=False)
                    
                    assert (coded_coe_num == (height + pad_h) * (width + pad_w) * 3)

                    for i in range(trans_steps):
                        j = trans_steps-i-1
                        LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j], j)
                    recon = LL
                    recon = yuv2rgb(recon.permute(1, 0, 2, 3))
                    recon = recon[:, :, 0:height, 0:width]
                    
                    blocks_h_rec.append(recon)
                    print(f'The {block_idx} block finished')
                    block_idx += 1
                
                blocks_rec.append(blocks_h_rec)
            
            recon = concat(blocks_rec)

        return recon