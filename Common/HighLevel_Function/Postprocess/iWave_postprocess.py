import torch
import torch.nn as nn
import numpy as np
from PIL import Image
from Common.HighLevel_Function.Postprocess.RestormerPostprocessing import Restormer
import os

class iWave_Postprocess(nn.Module):
    def __init__(self,  **kwargs):
        super().__init__()
        self.model_post = None
        self.model_postGAN = None

    def decode(self, img_array, header, recon_path, ckptdir):

        height = header.picture_size_h
        width = header.picture_size_w

        with torch.no_grad():
            if header.coding_tool.filtering_model_id == 1:
            
                recon = self.model_post(img_array)
            
                if height * width > 1080 * 1920:
                    h_list = [0, height // 2, height]
                    w_list = [0, width // 2, width]
                    k_ = 2
                else:
                    h_list = [0, height]
                    w_list = [0, width]
                    k_ = 1
                gan_rgb_post = torch.zeros_like(recon)
                for _i in range(k_):
                    for _j in range(k_):
                        pad_start_h = max(h_list[_i] - 64, 0) - h_list[_i]
                        pad_end_h = min(h_list[_i + 1] + 64, height) - h_list[_i + 1]
                        pad_start_w = max(w_list[_j] - 64, 0) - w_list[_j]
                        pad_end_w = min(w_list[_j + 1] + 64, width) - w_list[_j + 1]
                        tmp = self.model_postGAN(recon[:, :, h_list[_i] + pad_start_h:h_list[_i + 1] + pad_end_h,
                                                     w_list[_j] + pad_start_w:w_list[_j + 1] + pad_end_w])
                        gan_rgb_post[:, :, h_list[_i]:h_list[_i + 1], w_list[_j]:w_list[_j + 1]] = tmp[:, :,
                                                                                                   -pad_start_h:tmp.size()[
                                                                                                                    2] - pad_end_h,
                                                                                                   -pad_start_w:tmp.size()[
                                                                                                                    3] - pad_end_w]
                recon = gan_rgb_post
            else:
                model_lambdas = [0.2, 0.08, 0.024, 0.004, 0.001]
                device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
                restormer = Restormer().to(device)
                model_name=os.path.join(ckptdir,
                    f'{str(model_lambdas[header.parameter_set_id])}_post.ckpt')
                print('load model '+model_name)
                restormer.load_state_dict(torch.load(model_name, map_location=device))
                recon = img_array/255.
                recon_pad = F.pad(recon, (8, 8, 8, 8), 'reflect')
                rgb_post = torch.zeros_like(recon_pad)
                _,_,H,W = recon_pad.shape

                POST_CTU_W_SIZE = 2**(header.log2_slice_size_w_minus6 + 3)
                POST_CTU_H_SIZE = 2 ** (header.log2_slice_size_h_minus6 + 3)

                for h in range(0, H, POST_CTU_H_SIZE):
                    for w in range(0, W, POST_CTU_W_SIZE):
                        block = torch.zeros((1, 3, POST_CTU_H_SIZE + 16, POST_CTU_W_SIZE + 16)).cuda()
                        block[:, :, :min(POST_CTU_H_SIZE + 16, H - h), :min(POST_CTU_W_SIZE + 16, W - w)] = recon_pad[:, :,
                                                                                                        h:min(H,
                                                                                                              h + POST_CTU_H_SIZE + 16),
                                                                                                        w:min(W,
                                                                                                              w + POST_CTU_W_SIZE + 16)]
                        with torch.no_grad():
                            block = restormer(block)
                        for x in range(16):
                            if (w != 0):
                                block[:, :, :, x] = block[:, :, :, x] * x / 15
                            if (h != 0):
                                block[:, :, x, :] = block[:, :, x, :] * x / 15
                            if (w != W - POST_CTU_W_SIZE):
                                block[:, :, :, -(x + 1)] = block[:, :, :, -(x + 1)] * x / 15
                            if (h != H - POST_CTU_H_SIZE):
                                block[:, :, -(x + 1), :] = block[:, :, -(x + 1), :] * x / 15
                        rgb_post[:, :, h:min(H, h + POST_CTU_H_SIZE + 16), w:min(W, w + POST_CTU_W_SIZE + 16)] += block[:,
                                                                                                              :,
                                                                                                              :min(
                                                                                                                  POST_CTU_H_SIZE + 16,
                                                                                                                  H - h),
                                                                                                              :min(
                                                                                                                  POST_CTU_W_SIZE + 16,
                                                                                                                  W - w)]

                recon = rgb_post[:, :, 8:-8, 8:-8] * 255.

        recon = torch.clamp(torch.round(recon), 0., 255.)
        recon = recon[0, :, :, :]
        recon = recon.permute(1, 2, 0)
        recon = recon.cpu().data.numpy().astype(np.uint8)
        img = Image.fromarray(recon, 'RGB')
        img.save(recon_path)
