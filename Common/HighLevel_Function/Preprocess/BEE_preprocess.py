from Common.utils.bee_utils.csc import RGB2YCbCr
from Common.utils.bee_utils.tensorops import pad, resizeTensor
from Common.utils.bee_utils.testfuncs import readPngToTorchIm
import torch.nn as nn


class BEE_Preprocess(nn.Module):
    def __init__(self):
        super().__init__()

    def RGB2YUV(self, imArrayRGB):
        imArray = RGB2YCbCr()(imArrayRGB)
        return imArray

    def ImageResample(self, x_org, picture, device):
        h0, w0 = x_org.size(2), x_org.size(3)
        hr = picture.picture_header.coding_tool.resized_size_h
        wr = picture.picture_header.coding_tool.resized_size_w
        x = x_org if ((hr is None and wr is None) or (hr == h0 and wr == w0)) else resizeTensor(x_org, [hr, wr])
        x = x.to(device)
        return x

    def ImagePadding(self, x):
        p = 64  # maximum 6 strides of 2
        x = pad(x, p)
        return x

    def update(self, picture, bit_depth, bit_shift):
        picture.picture_header.output_bit_depth = 1 if bit_depth == 8 else 2
        picture.picture_header.coding_tool.output_bit_shift = bit_shift
        return picture

    def encode(self, img, picture, device):
        x_org, bit_depth, bit_shift = readPngToTorchIm(img)
        new_picture = self.update(picture, bit_depth, bit_shift)
        x_org.to(device)
        x_resample = self.ImageResample(x_org, picture, device)
        x_pad = self.ImagePadding(x_resample)
        x_pad.to(device)
        imArray = self.RGB2YUV(x_pad)
        return [imArray, x_pad, new_picture]
