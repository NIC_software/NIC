import torch.nn as nn

class BasePreprocess(nn.Module):
    def __init__(self):
        super().__init__()
        
    def decode(self, image, header, device):
        # return x (example: x_yuv)
        raise NotImplementedError
        