from Common.utils.bee_utils.tensorops import crop, update_registered_buffers
import Common.models.BEE_models.online as on
from Common.models.BEE_models.ans import BufferedRansEncoder
from Common.models.BEE_models.entropy_models import EntropyBottleneck, GaussianConditional
from Common.models.BEE_models.quantmodule import quantHSS
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockUpsample,
    subpel_conv3x3,
    MaskedConv2d,
    conv,
    deconv,
)
import torch
import torch.nn as nn
import torch.nn.functional as F
import einops
import math

from Common.models.BEE_models.ssc import CSSC

###
from Common.models.NIC_models.basic_module import Non_local_Block, ResBlock, ScalingNet

class Dec(nn.Module):
    def __init__(self, input_features, N1, M, M1):
        super(Dec, self).__init__()

        self.N1 = N1
        self.M = M
        self.M1 = M1
        self.input = input_features
        self.trunk1 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1))
        self.mask1 = nn.Sequential(Non_local_Block(self.M, self.M // 2), ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 1, 1, 0))

        self.up1 = nn.ConvTranspose2d(M, M, 5, 2, 2, 1)
        self.trunk2 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.ConvTranspose2d(M, M, 5, 2, 2, 1))
        
        self.trunk3 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.ConvTranspose2d(M, 2*self.M1, 5, 2, 2, 1))
        
        self.trunk4 = nn.Sequential(ResBlock(2*self.M1, 2*self.M1, 3, 1, 1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                    ResBlock(2*self.M1, 2*self.M1, 3, 1, 1))
        self.mask2 = nn.Sequential(Non_local_Block(2*self.M1, self.M1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   ResBlock(2*self.M1, 2*self.M1, 3, 1, 1), nn.Conv2d(2*self.M1, 2*self.M1, 1, 1, 0))
        self.trunk5 = nn.Sequential(nn.ConvTranspose2d(2*M1, M1, 5, 2, 2, 1), ResBlock(self.M1, self.M1, 3, 1, 1), ResBlock(self.M1, self.M1, 3, 1, 1),
                                    ResBlock(self.M1, self.M1, 3, 1, 1))

        self.conv1 = nn.Conv2d(self.M1, self.input, 5, 1, 2)

    def forward(self, x, lambda_rd=None):
        x1 = self.trunk1(x)*F.sigmoid(self.mask1(x))+x
        x1 = self.up1(x1)
        x2 = self.trunk2(x1)
        x3 = self.trunk3(x2)
        x4 = self.trunk4(x3)+x3
        #print (x4.size())
        x5 = self.trunk5(x4)
        output = self.conv1(x5)
        return output

class BEE_Symbol2Bin(nn.Module):
    SCALES_MIN = 0.11
    SCALES_MAX = 256
    SCALES_LEVELS = 64

    def __init__(self, N=192, M=192, Quantized=True, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.N = int(N)
        self.M = int(M)
        self.Quantized = Quantized
        self.waveShift = 1
        self.numthreads_min = 50
        self.numthreads_max = 100
        self.numfilters = [0, 0, 0]
        self.filterCoeffs1 = []
        self.filterCoeffs2 = []
        self.filterCoeffs3 = []
        self.likely = None
        self.channelOffsetsTool = False
        self.encChannelOffsets = []
        self.offsetSplit_w = 0
        self.offsetSplit_h = 0
        self.DeterminismSpeedup = True
        self.DoublePrecisionProcessing = False
        self.encParams = None
        self.gaussian_conditional = GaussianConditional(None)
        self.entropy_bottleneck = EntropyBottleneck(self.N)
        self.y_q_full = None
        self.kernel_height = 3
        self.kernel_width = 4
        
        self.N1 = int(192)
        self.N2 = int(128)
        self.M0 = int(192)                         
        self.M1 = int(96)
        self.input_features = int(3)          
        self.decoder = Dec(self.input_features, self.N1, self.M0, self.M1)
        
        self.cssc = CSSC(self.M0)

        self.h_s = nn.Sequential(
            deconv(N, M, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            deconv(M, M * 3 // 2, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 3 // 2, M * 2, stride=1, kernel_size=3, device=device),
        )
        if self.Quantized:
            self.h_s_scale = quantHSS(self.N, self.M, vbit=16, cbit=16, use_float=False, device=device)
            self.sbit = 12
        else:
            self.h_s_scale = nn.Sequential(
                deconv(N, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                deconv(M, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
            )

    def get_mask(self, scales_hat, filtercoeffs):
        mode = filtercoeffs["mode"]
        thr = filtercoeffs["thr"]
        block_size = filtercoeffs["block_size"]
        greater = filtercoeffs["greater"]
        scale = filtercoeffs["scale"]
        channels = filtercoeffs["channels"]
        mask = None
        self.likely = torch.abs(scales_hat)

        _, _, h, w = scales_hat.shape
        h_pad = ((h + block_size - 1) // block_size) * block_size - h
        w_pad = ((w + block_size - 1) // block_size) * block_size - w
        likely = F.pad(self.likely, (0, w_pad, 0, h_pad), value=1)
        if mode == 1:  # minpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = -(maxpool(-likely))
        if mode == 2:  # avgpool
            avgpool = torch.nn.AvgPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = avgpool(likely)
        if mode == 3:  # maxpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = (maxpool(likely))

        if greater:
            mask = (likely > thr)
        else:
            mask = (likely < thr)
        if mode == 4:  # maxpool
            for i in range(192):
                if i in channels:
                    pass
                else:
                    mask[:, i, :, :] = False
        mask = einops.repeat(mask, 'a b c d -> a b (c repeat1) (d repeat2)', repeat1=block_size, repeat2=block_size)
        mask = F.pad(mask, (0, -w_pad, 0, -h_pad), value=1)
        return mask, scale

    def HyperDecoding(self, z_hat):
        params = self.h_s(z_hat)
        return params

    def HyperScaleDecoding(self, z_hat):
        scales_hat = self.h_s_scale(z_hat)
        return scales_hat

    def y_refiner(self, y, z, x_true_org, quality, totalIte, params, scales_hat):
        criterion = on.RateDistortionLoss(quality)
        updater = on.latentUpdater(y)
        optimizer = torch.optim.Adagrad([{'params': updater.parameters(), 'lr': 0.03}])
        _, _, h, w = x_true_org.shape
        y = updater()
        y_best = y.clone()
        y_hat = y.clone()
        loss_best = 10000000000000
        for i in range(8):
            with torch.set_grad_enabled(False):
                _, w_hat, _ = self.cssc.encode(y.clone(), params, scales_hat, self.filterCoeffs1)
                   
                mask1 = []
                scale1 = []
                for i in range(self.numfilters[0]):
                    mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs1[i]))
                    mask1.append(mask)
                    scale1.append(scale)

                for i in range(self.numfilters[0] - 1, -1, -1):
                    w_hat = torch.where(mask1[i], w_hat / scale1[i][0], w_hat)
            
            with torch.set_grad_enabled(True):
                w_hat_new = y - y.detach() + w_hat.detach()
                y_new, means_hat = self.cssc.decode(w_hat_new, params.detach())
                
                x_hat = self.decoder(y_new)

                _, y_likelihoods = self.gaussian_conditional(y, scales_hat, means=means_hat)

                if self.gaussian_conditional.use_likelihood_bound:
                    y_likelihoods = self.gaussian_conditional.likelihood_lower_bound(y_likelihoods)
                x_hat = crop(x_hat, (h, w))
                measurements = {"x_hat": x_hat, "likelihoods": {"y": y_likelihoods, "z": []}}
                loss = criterion(measurements, x_true_org)
                optimizer.zero_grad()
                loss["loss"].backward()
                optimizer.step()
                if loss_best > loss["loss"].item():
                    loss_best = loss["loss"].item()
                    y_best = y.clone()
                optimizer.param_groups[0]['lr'] = optimizer.param_groups[0]['lr'] / 1.2
        return y_best, z

    def Latent_refiner(self, x, y, h, w, z, yuvd, device, quality, numIte, params, scales_hat):
        x_true_org = crop(yuvd, [h, w])
        x_true_org = x_true_org.to(device)
        if x.shape[2] * x.shape[3] < 9000000:
            y, _ = self.y_refiner(y.clone(), z.clone(), x_true_org, quality, numIte, params.clone(), scales_hat.clone())
        return y

    def update(self, header):
        self.numfilters = [header.picture_header.coding_tool.mask_scale.num_adaptive_quant_params,
                           header.picture_header.coding_tool.mask_scale.num_block_based_skip_params,
                           header.picture_header.coding_tool.mask_scale.num_latent_post_process_params]
        if self.numfilters[0]:
            self.filterCoeffs1 = header.picture_header.coding_tool.mask_scale.filterList[0:self.numfilters[0]]
        if self.numfilters[1]:
            self.filterCoeffs2 = header.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0]:self.numfilters[0] + self.numfilters[1]]
        if self.numfilters[2]:
            self.filterCoeffs3 = header.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0] + self.numfilters[1]:]
        self.DeterminismSpeedup = header.picture_header.coding_tool.deterministic_processing_flag
        self.DoublePrecisionProcessing = header.picture_header.coding_tool.double_precision_processing_flag
        self.channelOffsetsTool = header.picture_header.coding_tool.adaptive_offset.adaptive_offset_enabled_flag
        self.decChannelOffsets = header.picture_header.coding_tool.adaptive_offset.offsetList
        self.offsetSplit_w = header.picture_header.coding_tool.adaptive_offset.num_horizontal_split
        self.offsetSplit_h = header.picture_header.coding_tool.adaptive_offset.num_vertical_split

    def entropy_update(self, scale_table=None, force=False):
        if scale_table is None:
            scale_table = self.get_scale_table()
        u1 = self.gaussian_conditional.update_scale_table(scale_table, force=force)

        u2 = False
        for m in self.children():
            if not isinstance(m, EntropyBottleneck):
                continue
            rv = m.update(force=force)
            u2 |= rv
        u1 |= u2
        return u1

    def get_scale_table(self, MIN=SCALES_MIN, MAX=SCALES_MAX, levels=SCALES_LEVELS):
        return torch.exp(torch.linspace(math.log(MIN), math.log(MAX), levels))

    def load_state_dict(self, state_dict, strict=False):
        update_registered_buffers(
            self.gaussian_conditional,
            "gaussian_conditional",
            ["_quantized_cdf", "_offset", "_cdf_length", "scale_table"],
            state_dict,
        )
        update_registered_buffers(
            self.entropy_bottleneck,
            "entropy_bottleneck",
            ["_quantized_cdf", "_offset", "_cdf_length"],
            state_dict)
        state_dict_new = state_dict.copy()
        state_dict = state_dict_new
        super().load_state_dict(state_dict, strict=strict)
        self.entropy_update()

    def Decoupled_net(self,y,params,scales_hat):
        if y.is_cuda:
            self.cssc.half()
            y = y.half()
            params = params.half()
            y_hat, w_hat, means_hat = self.cssc.encode(y, params, scales_hat, self.filterCoeffs1)
            y_hat = y_hat.to(torch.float32)
            w_hat = w_hat.to(torch.float32)
        else:
            y_hat, w_hat, means_hat = self.cssc.encode(y, params, scales_hat, self.filterCoeffs1)

        height = y.size(2)
        width = y.size(3)
        y_org = y.clone()

        # Start of channel offsetting
        if self.channelOffsetsTool:
            wpad = ((width + self.offsetSplit_w - 1) // self.offsetSplit_w) * self.offsetSplit_w - width
            hpad = ((height + self.offsetSplit_h - 1) // self.offsetSplit_h) * self.offsetSplit_h - height
            kernel_w = (width + wpad) // self.offsetSplit_w
            kernel_h = (height + hpad) // self.offsetSplit_h
            diff = F.pad(y_org - y_hat, (0, wpad, 0, hpad))
            avg = torch.nn.AvgPool2d(kernel_size = (kernel_h, kernel_w), stride = (kernel_h, kernel_w), padding = 0)
            self.encChannelOffsets = avg(diff).squeeze()
            if self.offsetSplit_h == 1:
                self.encChannelOffsets.unsqueeze_(1)
            if self.offsetSplit_w == 1:
                self.encChannelOffsets.unsqueeze_(2)
            self.encChannelOffsets = self.encChannelOffsets.permute(1, 2, 0)
        # End of channel offsetting
        
        mask2 = torch.ones_like(scales_hat, dtype = torch.bool)
        for i in range(self.numfilters[1]):
            mask, _ = self.get_mask(scales_hat, dict(self.filterCoeffs2[i]))
            mask2 = mask2 * mask

        mask1 = []
        scale1 = []
        for i in range(self.numfilters[0]):
            mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs1[i]))
            mask1.append(mask)
            scale1.append(scale)

        for i in range(self.numfilters[0]):
            if self.Quantized:
                scales_hat = scales_hat.double()
                scales_hat[mask1[i]] *= self.h_s_scale.h_s_scale[-1].scale_v
                scales_hat[mask1[i]].round().clamp(-(2 ** (self.h_s_scale.h_s_scale[-1].abit - 1)),
                                                2 ** (self.h_s_scale.h_s_scale[-1].abit - 1) - 1)
                scalesfactor = round(scale1[i][0] * 2 ** self.sbit)
                scales_hat[mask1[i]] *= scalesfactor
                scales_hat[mask1[i]] /= (self.h_s_scale.h_s_scale[-1].scale_v * (2 ** self.sbit))
                scales_hat = scales_hat.float()
            else:
                scales_hat[mask1[i]] *= scale1[i][0]

        w_hat = w_hat[mask2]
        scales_hat = scales_hat[mask2]
        
        symbols_list = w_hat.to('cpu').detach().numpy()
        indexes_full = self.gaussian_conditional.build_indexes(scales_hat)
        indexes_list = indexes_full.to('cpu').numpy()
        return symbols_list, indexes_list

    def encode(self, forward_trans_varlist, header, device):
        rate2quality = {0.75: 6, 0.5: 4, 0.25: 3, 0.12: 2, 0.06: 1}
        x, y, z, yuvd = forward_trans_varlist[3], forward_trans_varlist[0], forward_trans_varlist[1], \
        forward_trans_varlist[2]
        rate = header.picture_header.coding_tool.rate
        numIte = header.picture_header.coding_tool.numIte
        quality_mapped = rate2quality[rate]
        torch.backends.cudnn.deterministic = True
        z_strings = self.entropy_bottleneck.compress(z)
        z_hat = self.entropy_bottleneck.decompress(z_strings, z.size()[-2:])
        params = self.HyperDecoding(z_hat)
        scales_hat = self.HyperScaleDecoding(z_hat)
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True
        # y = self.Latent_refiner(x, y,
        #                         header.picture_header.coding_tool.resized_size_h,
        #                         header.picture_header.coding_tool.resized_size_w,
        #                         z, yuvd, device, quality_mapped, numIte, params, scales_hat)
        symbols_list, indexes_list = self.Decoupled_net(y, params, scales_hat)
        header.picture_header.coding_tool.adaptive_offset.offsetList = self.encChannelOffsets
        return [symbols_list, indexes_list, forward_trans_varlist[1]]
