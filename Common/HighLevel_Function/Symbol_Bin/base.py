import torch.nn as nn

class BaseSymbol2Bin(nn.Module):
    def __init__(self):
        super().__init__()
        
    def decode(self, forward_trans_varlist, header):
        # return quantized latent y_hat (example: y_hat)
        raise NotImplementedError
        