import math

import torch
import torch.nn as nn
import torch.nn.functional as F
import einops

from Common.models.BEE_models.ans import BufferedRansEncoder, RansDecoder
from Common.models.BEE_models.entropy_models import EntropyBottleneck, GaussianConditional
from Common.models.BEE_models.quantmodule import quantHSS
import Common.models.BEE_models.online as on
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockUpsample,
    ResidualBlockWithStride,
    conv3x3,
    subpel_conv3x3,
    MaskedConv2d,
    conv,
    deconv,
)

from Common.utils.bee_utils.csc import RGB2YCbCr, YCbCr2RGB
from Common.utils.bee_utils.tensorops import update_registered_buffers, crop

# 
import torch.nn.functional as F

from Common.models.BEE_models.basic_module import Non_local_Block, ResBlock, ScalingNet
from Common.models.BEE_models.ssc import CSSC
__all__ = [
    "QuantYUV444Decoupled",
    "YUV444DecoupledV0",
]

# From Balle's tensorflow compression examples
SCALES_MIN = 0.11
SCALES_MAX = 256
SCALES_LEVELS = 64

MeanAndResidualScale = True


def get_scale_table(min=SCALES_MIN, max=SCALES_MAX, levels=SCALES_LEVELS):
    return torch.exp(torch.linspace(math.log(min), math.log(max), levels))

class Enc(nn.Module):
    def __init__(self, num_features, N1, N2, M, M1):
        #input_features = 3, N1 = 192, N2 = 128, M = 192, M1 = 96
        super(Enc, self).__init__()
        self.N1 = int(N1)
        self.N2 = int(N2)
        self.M = int(M)
        self.M1 = int(M1)
        self.n_features = int(num_features)

        self.conv1 = nn.Conv2d(self.n_features, self.M1, 5, 1, 2)
        self.trunk1 = nn.Sequential(ResBlock(self.M1, self.M1, 3, 1, 1), ResBlock(
            self.M1, self.M1, 3, 1, 1), nn.Conv2d(self.M1, 2*self.M1, 5, 2, 2))

        self.down1 = nn.Conv2d(2*self.M1, self.M, 5, 2, 2)
        self.trunk2 = nn.Sequential(ResBlock(2*self.M1, 2*self.M1, 3, 1, 1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                    ResBlock(2*self.M1, 2*self.M1, 3, 1, 1))
        self.mask1 = nn.Sequential(Non_local_Block(2*self.M1, self.M1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   ResBlock(
                                       2*self.M1, 2*self.M1, 3, 1, 1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   nn.Conv2d(2*self.M1, 2*self.M1, 1, 1, 0))

        self.trunk3 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 5, 2, 2))


        self.trunk4 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 5, 2, 2))

        self.trunk5 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1))
        self.mask2 = nn.Sequential(Non_local_Block(self.M, self.M // 2), ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 1, 1, 0))

    def forward(self, x, lambda_rd=None):

        x1 = self.conv1(x)
        x2 = self.trunk1(x1)
        x3 = self.trunk2(x2) + x2
        x3 = self.down1(x3)
        x4 = self.trunk3(x3)
        x5 = self.trunk4(x4)
        x6 = self.trunk5(x5) * F.sigmoid(self.mask2(x5)) + x5
        return x6

class Dec(nn.Module):
    def __init__(self, input_features, N1, M, M1):
        super(Dec, self).__init__()

        self.N1 = N1
        self.M = M
        self.M1 = M1
        self.input = input_features

        self.trunk1 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1))
        self.mask1 = nn.Sequential(Non_local_Block(self.M, self.M // 2), ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1), nn.Conv2d(self.M, self.M, 1, 1, 0))

        self.up1 = nn.ConvTranspose2d(M, M, 5, 2, 2, 1)

        self.trunk2 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.ConvTranspose2d(M, M, 5, 2, 2, 1))

        self.trunk3 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1), ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1), nn.ConvTranspose2d(M, 2*self.M1, 5, 2, 2, 1))

        self.trunk4 = nn.Sequential(ResBlock(2*self.M1, 2*self.M1, 3, 1, 1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                    ResBlock(2*self.M1, 2*self.M1, 3, 1, 1))
        self.mask2 = nn.Sequential(Non_local_Block(2*self.M1, self.M1), ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   ResBlock(2*self.M1, 2*self.M1, 3, 1, 1),
                                   ResBlock(2*self.M1, 2*self.M1, 3, 1, 1), nn.Conv2d(2*self.M1, 2*self.M1, 1, 1, 0))

        self.trunk5 = nn.Sequential(nn.ConvTranspose2d(2*M1, M1, 5, 2, 2, 1), ResBlock(self.M1, self.M1, 3, 1, 1), ResBlock(self.M1, self.M1, 3, 1, 1),
                                    ResBlock(self.M1, self.M1, 3, 1, 1))

        self.conv1 = nn.Conv2d(self.M1, self.input, 5, 1, 2)

    def forward(self, x, lambda_rd=None):
        x1 = self.trunk1(x)*F.sigmoid(self.mask1(x))+x
        x1 = self.up1(x1)
        x2 = self.trunk2(x1)
        x3 = self.trunk3(x2)
        x4 = self.trunk4(x3)+x3
        x5 = self.trunk5(x4)
        output = self.conv1(x5)
        return output
    

class QuantYUV444Decoupled(nn.Module):
    def __init__(self, N=192, M=192, refine=4, init_weights=True, Quantized=False, oldversion=False, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        if init_weights:
            self._initialize_weights()
        self.N = int(N)
        self.M = int(M)
        self.Quantized = Quantized
        self.refine = refine
        self.noise = 0
        self.numthreads_min = 50
        self.numthreads_max = 100
        self.waveShift = 1
        self.numfilters = [0, 0, 0]
        self.filterCoeffs1 = []
        self.filterCoeffs2 = []
        self.filterCoeffs3 = []
        self.decSplit1 = 1
        self.decSplit2 = 1
        self.likely = None
        self.channelOffsetsTool = False
        self.encChannelOffsets = []
        self.decChannelOffsets = []
        self.offsetSplit_w = 0
        self.offsetSplit_h = 0
        self.DeterminismSpeedup = True
        self.DoublePrecisionProcessing = False
        self.encParams = None
        self.encSkip = False
        self.ACSkip = False
        self.y_q_full = None
        self.encOutput = None
        self.encCompleteSkip = False
        self.decCompleteSkip = False
        self.oldversion = oldversion
        self.decParameters = None
        self.kernel_height = 3
        self.kernel_width = 4
        self.yuv2rgb = YCbCr2RGB()
        self.rgb2yuv = RGB2YCbCr()
        
        # entropy module
        self.N1 = int(192)
        self.N2 = int(128)
        self.M0 = int(192)
        self.M1 = int(96)
        self.input_features = int(3)  
        self.cssc = CSSC(self.M0)

        self.gaussian_conditional = GaussianConditional(None)
        self.entropy_bottleneck = EntropyBottleneck(self.N)

        if not kwargs.get("decoderOnly"):
            self.encoder = Enc(self.input_features, self.N1, self.N2, self.M0, self.M1)
            self.h_a = nn.Sequential(
                conv(M, N, stride=1, kernel_size=3),
                nn.LeakyReLU(inplace=True),
                conv(N, N, stride=2, kernel_size=5),
                nn.LeakyReLU(inplace=True),
                conv(N, N, stride=2, kernel_size=5),
            )
            
        self.decoder = Dec(self.input_features, self.N1, self.M0, self.M1)
        self.h_s = nn.Sequential(
            deconv(N, M, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            deconv(M, M * 3 // 2, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 3 // 2, M * 2, stride=1, kernel_size=3, device=device),
        )
        if self.Quantized:
            self.h_s_scale = quantHSS(self.N, self.M, vbit=16, cbit=16, use_float=False, device=device)
            self.sbit = 12
        else:
            self.h_s_scale = nn.Sequential(
                deconv(N, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                deconv(M, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
            )

    def aux_loss(self):
        aux_loss = sum(
            m.loss() for m in self.modules() if isinstance(m, EntropyBottleneck)
        )
        return aux_loss

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.ConvTranspose2d)):
                nn.init.kaiming_normal_(m.weight)
                if m.bias is not None:
                    nn.init.zeros_(m.bias)

    def update(self, scale_table=None, force=False):
        if scale_table is None:
            scale_table = get_scale_table()
        u1 = self.gaussian_conditional.update_scale_table(scale_table, force=force)

        u2 = False
        for m in self.children():
            if not isinstance(m, EntropyBottleneck):
                continue
            rv = m.update(force=force)
            u2 |= rv
        u1 |= u2
        return u1

    def load_state_dict(self, state_dict):
        state_dict_new = state_dict.copy()
        # exclude = ["g_s.6", "g_s.7", "g_s.8"]
        # include = ["g_s_extension.0", "g_s_extension.1", "g_s_extension.2"]
        # for k in state_dict:
        #     for idx, j in enumerate(exclude):
        #         if j in k:
        #             k_new = k.replace(j, include[idx])
        #             state_dict_new[k_new] = state_dict_new[k]
        #             del state_dict_new[k]
        state_dict = state_dict_new
        update_registered_buffers(
            self.gaussian_conditional,
            "gaussian_conditional",
            ["_quantized_cdf", "_offset", "_cdf_length", "scale_table"],
            state_dict,
        )
        update_registered_buffers(
            self.entropy_bottleneck,
            "entropy_bottleneck",
            ["_quantized_cdf", "_offset", "_cdf_length"],
            state_dict)
        super().load_state_dict(state_dict, strict=False)

    @property
    def downsampling_factor(self) -> int:
        return 2 ** (4 + 2)

    @classmethod
    def from_state_dict(cls, state_dict):
        """Return a new model instance from `state_dict`."""
        N = state_dict["g_a.0.weight"].size(0)
        M = state_dict["g_a.6.weight"].size(0)
        net = cls(N, M)
        net.load_state_dict(state_dict)
        return net

    @staticmethod
    def splitFunc(x, func, splitNum, pad, crop):
        _, _, _, w = x.shape
        w_step = ((w + splitNum - 1) // splitNum)
        pp = []
        for i in range(splitNum):
            start_offset = pad if i > 0 else 0
            start_crop = crop if i > 0 else 0
            end_offset = pad if i < (splitNum - 1) else 0
            end_crop = crop if i < (splitNum - 1) else 0
            dummy = func(x[:, :, :, (w_step * i) - start_offset:w_step * (i + 1) + end_offset])
            dummy = dummy[:, :, :, start_crop:]
            if end_crop > 0:
                dummy = dummy[:, :, :, :-end_crop]
            pp.append(dummy)
        x_hat = torch.cat(pp, dim=3)
        return x_hat

    def get_mask(self, scales_hat, filtercoeffs):
        mode = filtercoeffs["mode"]
        thr = filtercoeffs["thr"]
        block_size = filtercoeffs["block_size"]
        greater = filtercoeffs["greater"]
        scale = filtercoeffs["scale"]
        channels = filtercoeffs["channels"]
        mask = None
        self.likely = torch.abs(scales_hat)

        _, _, h, w = scales_hat.shape
        h_pad = ((h + block_size - 1) // block_size) * block_size - h
        w_pad = ((w + block_size - 1) // block_size) * block_size - w
        likely = F.pad(self.likely, (0, w_pad, 0, h_pad), value=1)
        if mode == 1:  # minpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = -(maxpool(-likely))
        if mode == 2:  # avgpool
            avgpool = torch.nn.AvgPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = avgpool(likely)
        if mode == 3:  # maxpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = (maxpool(likely))

        if greater:
            mask = (likely > thr)
        else:
            mask = (likely < thr)
        if mode == 4:  # maxpool
            for i in range(192):
                if i in channels:
                    pass
                else:
                    mask[:, i, :, :] = False
        mask = einops.repeat(mask, 'a b c d -> a b (c repeat1) (d repeat2)', repeat1=block_size, repeat2=block_size)
        mask = F.pad(mask, (0, -w_pad, 0, -h_pad), value=1)
        return mask, scale

    def proxyfunc(self, x):
        y = self.encoder(x)
        z = self.h_a(y)
        z_hat, _ = self.entropy_bottleneck(z)
        return z_hat

    def evalEstimate(self, x):
        y = self.encoder(x)
        z = self.h_a(y)
        _, z_likelihoods = self.entropy_bottleneck(z)
        z_strings = self.entropy_bottleneck.compress(z)
        z_hat = self.entropy_bottleneck.decompress(z_strings, z.size()[-2:])
        params = self.h_s(z_hat)
        _, _, h, w = y.shape

        y_hat, means_hat = self.cssc(y, params)

        scales_hat = self.h_s_scale(z_hat)

        _, y_likelihoods = self.gaussian_conditional(y, scales_hat, means=means_hat)
        x_hat = self.decoder(y_hat).clamp_(0, 1)
        measurements = {"x_hat": x_hat, "y_hat": y_hat, "likelihoods": {"y": y_likelihoods, "z": z_likelihoods}}
        return measurements

    def forward(self, x):
        y = self.encoder(x)
        z = self.h_a(y)
        z_hat, z_likelihoods = self.entropy_bottleneck(z)
        params = self.h_s(z_hat)
        
        y_hat, means_hat = self.cssc(y, params)

        scales_hat = self.h_s_scale(z_hat)

        _, y_likelihoods = self.gaussian_conditional(y, scales_hat, means=means_hat)
        x_hat = self.decoder(y_hat)

        return {
            "x_hat": x_hat,
            "y_hat": y_hat,
            "likelihoods": {"y": y_likelihoods, "z": z_likelihoods},
        }

    def forward_fineTune(self, x):
        with torch.no_grad():
            y = self.encoder(x)
            z = self.h_a(y)
            z_strings = self.entropy_bottleneck.compress(z)
            z_hat = self.entropy_bottleneck.decompress(z_strings, z.size()[-2:])
            params = self.h_s(z_hat)
            y_hat, means_hat = self.cssc(y, params)

        params = self.h_s(z_hat)
        scales_hat = self.h_s_scale(z_hat)
        _, y_likelihoods = self.gaussian_conditional(y, scales_hat, means=means_hat)
        x_hat = self.decoder(y_hat)
        return {
            "x_hat": x_hat,
            "likelihoods": {"y": y_likelihoods},
        }

    def refiner_init(self, y, z, quality, toUpdate):
        criterion = on.RateDistortionLoss(quality)
        updater = on.latentUpdater(toUpdate)
        optimizer = torch.optim.Adagrad([{'params': updater.parameters(), 'lr': 0.04}])
        params_est = self.h_s(z.round())
        y_hat, means = self.cssc(y, params_est)

        means = means.detach()
        return criterion, updater, optimizer, means

    def y_refiner(self, y, z, x_true_org, quality, totalIte):
        criterion = on.RateDistortionLoss(quality)
        updater = on.latentUpdater(y)
        optimizer = torch.optim.Adagrad([{'params': updater.parameters(), 'lr': 0.03}])
        _, _, h, w = x_true_org.shape
        y = updater()
        y_best = y.clone()
        with torch.set_grad_enabled(True):
            for i in range(totalIte):
                x_hat = self.decoder(y)
                x_hat = crop(x_hat, (h, w))
                measurements = {"x_hat": x_hat, "likelihoods": {"y": [], "z": []}}
                loss = criterion(measurements, x_true_org)
                optimizer.zero_grad()
                loss["loss"].backward()
                optimizer.step()
                y_best = y.clone()
                optimizer.param_groups[0]['lr'] = optimizer.param_groups[0]['lr'] / 3
        return y_best, z


class YUV444DecoupledV0(QuantYUV444Decoupled):
    def __init__(self, N=192, M=192, refine=4, **kwargs):
        super().__init__(N=N, M=N, refine=refine, **kwargs)
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.Quantized = False
        self.h_s_scale = nn.Sequential(
            deconv(N, M, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            deconv(M, M * 3 // 2, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 3 // 2, M * 2, stride=1, kernel_size=3, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 2, M * 5 // 3, stride=1, kernel_size=1, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 5 // 3, M * 4 // 3, stride=1, kernel_size=1, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 4 // 3, M * 3 // 3, stride=1, kernel_size=1, device=device),
        )