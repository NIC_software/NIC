'''
    Contributors: Haojie Liu, Tong Chen, Chuanmin Jia, Yihang Chen, Ziqing Ge
'''

import numpy as np
import torch
import torch.nn as nn
# import torch.nn.functional as f
from torch import Tensor
from Common.models.NIC_models.basic_module import ResBlock
from Common.models.NIC_models.functional_class import NormalCDF
from Common.models.NIC_models.gaussian_entropy_model import Distribution_for_entropy2
from typing import Any, Dict, Tuple
# import torch.nn.functional as F
import rans



class Space2Depth(nn.Module):
    """
    ref: https://github.com/huzi96/Coarse2Fine-PyTorch/blob/master/networks.py
    """

    def __init__(self, r=2):
        super().__init__()
        self.r = r

    def forward(self, x):
        r = self.r
        b, c, h, w = x.size()
        out_c = c * (r ** 2)
        out_h = h // r
        out_w = w // r
        x_view = x.view(b, c, out_h, r, out_w, r)
        x_prime = x_view.permute(0, 3, 5, 1, 2, 4).contiguous().view(b, out_c, out_h, out_w)
        return x_prime


def Demultiplexerv2(x):
    """
    See Supplementary Material: Figure 2.
    This operation can also implemented by slicing.
    """
    x_prime = Space2Depth(r=2)(x)

    _, C, _, _ = x_prime.shape
    y1_index = tuple(range(0, C // 4))
    y2_index = tuple(range(C * 3 // 4, C))
    y3_index = tuple(range(C // 4, C // 2))
    y4_index = tuple(range(C // 2, C * 3 // 4))

    y1 = x_prime[:, y1_index, :, :]
    y2 = x_prime[:, y2_index, :, :]
    y3 = x_prime[:, y3_index, :, :]
    y4 = x_prime[:, y4_index, :, :]

    return y1, y2, y3, y4


def conv(in_channels, out_channels, kernel_size=5, stride=2):
    return nn.Conv2d(
        in_channels,
        out_channels,
        kernel_size=kernel_size,
        stride=stride,
        padding=kernel_size // 2,
    )


class MaskConv3d(nn.Conv3d):
    def __init__(self, mask_type, in_ch, out_ch, kernel_size, stride, padding):
        super(MaskConv3d, self).__init__(in_ch, out_ch,
                                         kernel_size, stride, padding, bias=True)

        self.mask_type = mask_type
        ch_out, ch_in, k, k, k = self.weight.size()
        mask = torch.zeros(ch_out, ch_in, k, k, k)
        central_id = k*k*k//2+1
        current_id = 1
        if mask_type == 'A':
            for i in range(k):
                for j in range(k):
                    for t in range(k):
                        if current_id < central_id:
                            mask[:, :, i, j, t] = 1
                        else:
                            mask[:, :, i, j, t] = 0
                        current_id = current_id + 1
        if mask_type == 'B':
            for i in range(k):
                for j in range(k):
                    for t in range(k):
                        if current_id <= central_id:
                            mask[:, :, i, j, t] = 1
                        else:
                            mask[:, :, i, j, t] = 0
                        current_id = current_id + 1
        if mask_type == 'C':
            for i in range(k):
                for j in range(k):
                    for t in range(k):
                        if current_id < central_id:
                            mask[:, :, i, j, t] = 0
                        else:
                            mask[:, :, i, j, t] = 1
                        current_id = current_id + 1
        self.register_buffer('mask', mask)

    def forward(self, x):
        self.weight.data *= self.mask
        return super(MaskConv3d, self).forward(x)


class Maskb_resblock(nn.Module):
    def __init__(self, in_channel, out_channel, kernel_size, stride, padding):
        super(Maskb_resblock, self).__init__()
        self.in_ch = int(in_channel)
        self.out_ch = int(out_channel)
        self.k = int(kernel_size)
        self.stride = int(stride)
        self.padding = int(padding)
        self.activation = nn.ReLU()

        self.conv1 = MaskConv3d(
            'B', self.in_ch, self.out_ch, self.k, self.stride, self.padding)
        self.conv2 = MaskConv3d(
            'B', self.in_ch, self.out_ch, self.k, self.stride, self.padding)

    def forward(self, x):
        x1 = self.conv2(self.activation(self.conv1(x)))
        out = x + x1
        return out


class Resblock_3D(nn.Module):
    def __init__(self, in_channel, out_channel, kernel_size, stride, padding):
        super(Resblock_3D, self).__init__()
        self.in_ch = int(in_channel)
        self.out_ch = int(out_channel)
        self.k = int(kernel_size)
        self.stride = int(stride)
        self.padding = int(padding)

        self.conv1 = nn.Conv3d(self.in_ch, self.out_ch,
                               self.k, self.stride, self.padding)
        self.conv2 = nn.Conv3d(self.in_ch, self.out_ch,
                               self.k, self.stride, self.padding)
        self.activation = nn.ReLU()

    def forward(self, x):
        x1 = self.conv2(self.activation(self.conv1(x)))
        out = x + x1
        return out


class P_Model(nn.Module):
    def __init__(self, M):
        super(P_Model, self).__init__()
        self.context_p = nn.Sequential(ResBlock(M, M, 3, 1, 1), ResBlock(M, M, 3, 1, 1), ResBlock(M, M, 3, 1, 1),
                                       nn.Conv2d(M, 2 * M, 3, 1, 1))

    def forward(self, x):

        x = self.context_p(x)
        return x


class Weighted_Gaussian(nn.Module):
    def __init__(self, M):
        super(Weighted_Gaussian, self).__init__()
        self.conv1 = MaskConv3d('A', 1, 24, 11, 1, 5)

        self.conv2 = nn.Sequential(nn.Conv3d(25, 48, 1, 1, 0), nn.ReLU(), nn.Conv3d(48, 96, 1, 1, 0), nn.ReLU(),
                                   nn.Conv3d(96, 9, 1, 1, 0))
        self.conv3 = nn.Conv2d(M * 2, M, 3, 1, 1)

        self.gaussin_entropy_func = Distribution_for_entropy2()

    def forward(self, x, hyper):
        x = torch.unsqueeze(x, dim=1)
        hyper = torch.unsqueeze(self.conv3(hyper), dim=1)
        x1 = self.conv1(x)
        output = self.conv2(torch.cat((x1, hyper), dim=1))
        p3 = self.gaussin_entropy_func(torch.squeeze(x, dim=1), output)
        return p3, output


class MultistageMaskedConv2d(nn.Conv2d):
    def __init__(self, *args: Any, mask_type: str = "A", **kwargs: Any):
        super().__init__(*args, **kwargs)

        self.register_buffer("mask", torch.zeros_like(self.weight.data))

        if mask_type == 'A':
            self.mask[:, :, 0::2, 0::2] = 1
        elif mask_type == 'B':
            self.mask[:, :, 0::2, 1::2] = 1
            self.mask[:, :, 1::2, 0::2] = 1
        elif mask_type == 'C':
            self.mask[:, :, :, :] = 1
            self.mask[:, :, 1:2, 1:2] = 0
        elif mask_type =='A/':
            self.mask[:, :, 0::2, 1::2] = 1
            self.mask[:, :, 1::2, 0::2] = 1
            self.mask[:, :, 1, 1]=1
        elif mask_type =='B/':
            self.mask[:, :, 0::2, 0::2] = 1
            self.mask[:, :, 1, 1]=1
        elif mask_type =='C/':
            self.mask[:, :, 1, 1]=1
        else:
            raise ValueError(f'Invalid "mask_type" value "{mask_type}"')

    def forward(self, x: Tensor) -> Tensor:
        # TODO: weight assigment is not supported by torchscript
        self.weight.data *= self.mask
        return super().forward(x)


class Multistage(nn.Module):
    def __init__(self, chs):
        super(Multistage, self).__init__()
        self.cc_transforms = nn.ModuleList(
            [
                nn.Sequential(
                    conv(in_channels=chs * 2, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs // 2, kernel_size=3, stride=1)
                ),
                nn.Sequential(
                    conv(in_channels=chs * 2 + chs // 2, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs // 2, kernel_size=3, stride=1)
                )]
        )

        self.entropy_parameters = nn.ModuleList(
            [
                nn.Sequential(
                    conv(in_channels=chs * 3, out_channels=chs + chs // 3 * 4, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 4, out_channels=chs + chs // 3 * 2, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 2, out_channels=chs // 2 * 9, kernel_size=1, stride=1)
                ),
                nn.Sequential(
                    conv(in_channels=chs * 3, out_channels=chs + chs // 3 * 4, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 4, out_channels=chs + chs // 3 * 2, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 2, out_channels=chs // 2 * 9, kernel_size=1, stride=1)
                )
            ]
        )
        self.sc_transform_1 = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A'
            )
        ])
        
        self.sc_transform_1_plus = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A/'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A/'
            )
        ])
        
        self.sc_transform_2 = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B'
            )
        ])
        
        self.sc_transform_2_plus=nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B/'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B/'
            )
        ])
        
        self.sc_transform_3 = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C'
            )
        ])
        
        self.sc_transform_3_plus = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C/'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C/'
            )
        ])

        self.conv3 = nn.Conv2d(chs * 2, chs, 3, 1, 1)
        self.gaussin_entropy_func = Distribution_for_entropy2()
        self.chs = chs
        self.unshuffle = nn.PixelUnshuffle(2)
        self.softmax = nn.Softmax(dim=-1)
        self.dec = None
        self.normal_cdf = NormalCDF()
    
    def entropy_decode(self, header, param_out, stage_num, threshold):
        device = param_out.device
        Min_Main, Max_Main = header.min_main, header.max_main
        sample = np.arange(header.min_main, header.max_main + 1 + 1)
        precise = 16
        prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = [Demultiplexerv2(torch.chunk(param_out, 9, dim=1)[i].squeeze(1))[stage_num]
                                                                            for i in range(9)]

        b, c, h, w = mean0.shape
        if self.dec != None:
            not_sparsity = (torch.abs(scale0) > threshold) | (torch.abs(scale1) > threshold) | (
                     torch.abs(scale2) > threshold) | (torch.abs(mean0) > threshold) | (torch.abs(mean1) > threshold) | (
                                 torch.abs(mean2) > threshold)

            ones = torch.ones_like(scale0).to(device)
            len_nots = int(torch.sum(ones * not_sparsity).item())

            if len_nots==0:
                _y_main_q_res = torch.zeros((b, c, h, w)).to(mean0.device)
                y_main_q_res = torch.zeros((b, c, h * 2, w * 2)).to(mean0.device)
                #####

                if stage_num == 0:
                    h_index = 0
                    w_index = 0
                elif stage_num == 1:
                    h_index = 1
                    w_index = 1
                elif stage_num == 2:
                    h_index = 0
                    w_index = 1
                elif stage_num == 3:
                    h_index = 1
                    w_index = 0
                y_main_q_res[:, :, h_index::2, w_index::2] = _y_main_q_res
                return y_main_q_res

            params = [
                 Demultiplexerv2(torch.chunk(param_out, 9, dim=1)[i].squeeze(1))[stage_num]
                 for i in range(9)]

            nots = not_sparsity.view(-1).detach().cpu().numpy()

            for i in range(9):
                params[i] = params[i].view(1, -1)
                params[i] = params[i].detach().cpu().numpy()[:, nots]
                params[i] = torch.from_numpy(params[i]).to(device)

            prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = params

            probs = torch.stack([prob0, prob1, prob2], dim=-1)
            probs = self.softmax(probs)
             # process the scale value to positive non-zero

            sample = torch.FloatTensor(np.tile(sample, [1, len_nots, 1])).to(device)

            scale0 = torch.abs(scale0)
            scale1 = torch.abs(scale1)
            scale2 = torch.abs(scale2)
            scale0[scale0 < 1e-6] = 1e-6
            scale1[scale1 < 1e-6] = 1e-6
            scale2[scale2 < 1e-6] = 1e-6
            mean0 = torch.unsqueeze(mean0, dim=2)
            mean1 = torch.unsqueeze(mean1, dim=2)
            mean2 = torch.unsqueeze(mean2, dim=2)
            scale0 = torch.unsqueeze(scale0, dim=2)
            scale1 = torch.unsqueeze(scale1, dim=2)
            scale2 = torch.unsqueeze(scale2, dim=2)
             # 3 gaussian distributions
            lower = torch.zeros(1, len_nots, Max_Main - Min_Main + 2)
            lower0 = self.normal_cdf(sample - 0.5, scale0, mean0)
            lower1 = self.normal_cdf(sample - 0.5, scale1, mean1)
            lower2 = self.normal_cdf(sample - 0.5, scale2, mean2)
            for i in range(sample.shape[2]):
                lower[:, :, i] = probs[:, :, 0] * lower0[:, :, i] + \
                                 probs[:, :, 1] * lower1[:, :, i] + probs[:, :, 2] * lower2[:, :, i]

            cdf_m = lower.data.cpu().numpy() * ((1 << precise) - (Max_Main - Min_Main + 1))  # [1, c, h, w ,Max-Min+1]
            cdf_m = cdf_m.astype(np.int32) + \
                     sample.cpu().numpy().astype(np.int32) - Min_Main

            cdf_m = np.reshape(cdf_m, [len_nots, -1])
            _y_main_q_res = self.dec.decode_batch(cdf_m)

            not_sparsity = ones * not_sparsity
            not_sparsity = not_sparsity.view(-1).detach().cpu().numpy().tolist()

            spaned = []
            cnt = 0

            for i in range(len(not_sparsity)):
                if not_sparsity[i] == 0:
                    spaned.append(0)
                else:
                    spaned.append(_y_main_q_res[cnt]+Min_Main)
                    cnt += 1

            _y_main_q_res = torch.Tensor(spaned).reshape(b, c, h, w).to(mean0.device)
            
        else:
            _y_main_q_res = [Min_Main] * int(b*c*h*w)
            _y_main_q_res = torch.Tensor(_y_main_q_res).reshape(b, c, h, w).to(mean0.device)
        
        y_main_q_res = torch.zeros((b, c, h * 2, w * 2)).to(mean0.device)
        #####

        if stage_num == 0:
            h_index = 0
            w_index = 0
        elif stage_num == 1:
            h_index = 1
            w_index = 1
        elif stage_num == 2:
            h_index = 0
            w_index = 1
        elif stage_num == 3:
            h_index = 1
            w_index = 0
        y_main_q_res[:, :, h_index::2, w_index::2] = _y_main_q_res
        return y_main_q_res

    def single_slice_multistage_encode_decode(self,
                                       xq1: torch.Tensor,
                                       support_slices: torch.Tensor,
                                       params: torch.Tensor,
                                       slice_index: int,
                                       x1_predict,
                                       header,
                                       threshold) -> torch.Tensor:
        if xq1 is not None:
            # Encoder; Split out the decode slices
            y_main_q_res_1 = xq1.clone()
            y_main_q_res_1[:, :, 0::2, 1::2] = 0
            y_main_q_res_1[:, :, 1::2, :] = 0 # 0, 0
            y_main_q_res_2 = xq1.clone()
            y_main_q_res_2[:, :, 0::2, :] = 0
            y_main_q_res_2[:, :, 1::2, 0::2] = 0 # 1, 1
            y_main_q_res_3 = xq1.clone()
            y_main_q_res_3[:, :, 0::2, 0::2] = 0
            y_main_q_res_3[:, :, 1::2, :] = 0 # 0, 1
            y_main_q_res_4 = xq1.clone()
            y_main_q_res_4[:, :, 0::2, :] = 0
            y_main_q_res_4[:, :, 1::2, 1::2] = 0 # 1, 0
        cc_params = self.cc_transforms[slice_index](support_slices)
        sc_params_1 = torch.zeros_like(cc_params)
        sc_params = sc_params_1
        param_out_1 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_1[:, :, 0::2, 1::2] = 0
        param_out_1[:, :, 1::2, :] = 0
        if xq1 is None:
            # Decoder
            y_main_q_res_1 = self.entropy_decode(header=header, param_out=param_out_1, stage_num=0, threshold=threshold)
        y_0 = y_main_q_res_1 + x1_predict
        y_0[:, :, 0::2, 1::2] = 0
        y_0[:, :, 1::2, :] = 0
        
        sc_params_2 = self.sc_transform_1[slice_index](y_0)
        sc_params_2_plus = self.sc_transform_1_plus[slice_index](x1_predict)
        sc_params_2 = sc_params_2 + sc_params_2_plus
        
        sc_params_2[:, :, 0::2, :] = 0
        sc_params_2[:, :, 1::2, 0::2] = 0
        sc_params = sc_params_1 + sc_params_2

        param_out_2 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_2[:, :, 0::2, :] = 0
        param_out_2[:, :, 1::2, 0::2] = 0
        if xq1 is None:
            # Decoder
            y_main_q_res_2 = self.entropy_decode(header=header, param_out=param_out_2, stage_num=1, threshold=threshold)
        y_1 = (y_main_q_res_1 + y_main_q_res_2 + x1_predict).clone()
        y_1[:, :, 0::2, 1::2] = 0
        y_1[:, :, 1::2, 0::2] = 0
        
        sc_params_3 = self.sc_transform_2[slice_index](y_1)
        sc_params_3_plus = self.sc_transform_2_plus[slice_index](x1_predict)
        sc_params_3 = sc_params_3 + sc_params_3_plus
        
        sc_params_3[:, :, 0::2, 0::2] = 0
        sc_params_3[:, :, 1::2, :] = 0
        sc_params = sc_params_1 + sc_params_2 + sc_params_3
        param_out_3 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_3[:, :, 0::2, 0::2] = 0
        param_out_3[:, :, 1::2, :] = 0
        if xq1 is None:
            # Decoder
            y_main_q_res_3 = self.entropy_decode(header=header, param_out=param_out_3, stage_num=2, threshold=threshold)
        y_2 = (y_main_q_res_1 + y_main_q_res_2 + y_main_q_res_3 + x1_predict).clone()
        y_2[:, :, 1::2, 0::2] = 0
        
        sc_params_4 = self.sc_transform_3[slice_index](y_2)
        sc_params_4_plus = self.sc_transform_3_plus[slice_index](x1_predict)
        sc_params_4 = sc_params_4 + sc_params_4_plus
        
        sc_params_4[:, :, 0::2, :] = 0
        sc_params_4[:, :, 1::2, 1::2] = 0
        sc_params = sc_params_1 + sc_params_2 + sc_params_3 + sc_params_4
        param_out_4 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_4[:, :, 0::2, :] = 0
        param_out_4[:, :, 1::2, 1::2] = 0
        if xq1 is None:
            # Decoder
            y_main_q_res_4 = self.entropy_decode(header=header, param_out=param_out_4, stage_num=3, threshold=threshold)
        x1_recon = y_main_q_res_1 + y_main_q_res_2 + y_main_q_res_3 + y_main_q_res_4 + x1_predict
        param_out = param_out_1 + param_out_2 + param_out_3 + param_out_4
        return param_out, x1_recon
    
    def _compress_decompress(self, mode, xq1, params, x1_predict, header=None, threshold=None, dec=None):
        self.dec = dec
        channel_num = x1_predict.shape[1]
        if xq1 is None:
            xq_half_0 = xq_half_1 = None
        else:
            xq_half_0 = xq1[:, :channel_num // 2, :, :]
            xq_half_1 = xq1[:, channel_num // 2:, :, :]

        param_out_1, decode_half_1 = self.single_slice_multistage_encode_decode(xq1=xq_half_0,
                                                          support_slices=params,
                                                          params=params,
                                                          slice_index=0,
                                                          x1_predict=x1_predict[:,:channel_num // 2, :, :],
                                                          header=header,
                                                          threshold=threshold)
        support_slices = torch.cat([params, decode_half_1], dim=1)
        param_out_2, decode_half_2 = self.single_slice_multistage_encode_decode(xq1=xq_half_1,
                                                          support_slices=support_slices,
                                                          params=params,
                                                          slice_index=1,
                                                          x1_predict=x1_predict[:,channel_num // 2:, :, :],
                                                          header=header,
                                                          threshold=threshold)
        if mode == 'decode':
            return torch.cat((decode_half_1, decode_half_2), dim=1)
        elif mode == 'encode':
            prob_1_0, mean_1_0, scale_1_0, prob_1_1, mean_1_1, scale_1_1, prob_1_2, mean_1_2, scale_1_2 = \
            [torch.cat(Demultiplexerv2(torch.chunk(param_out_1, 9, dim=1)[i].squeeze(1)), dim=1) for i in range(9)]
            prob_2_0, mean_2_0, scale_2_0, prob_2_1, mean_2_1, scale_2_1, prob_2_2, mean_2_2, scale_2_2 = \
                [torch.cat(Demultiplexerv2(torch.chunk(param_out_2, 9, dim=1)[i].squeeze(1)), dim=1) for i in range(9)]
            context_out = torch.cat((prob_1_0, prob_2_0, mean_1_0, mean_2_0, scale_1_0, scale_2_0,
                                    prob_1_1, prob_2_1, mean_1_1, mean_2_1, scale_1_1, scale_2_1,
                                    prob_1_2, prob_2_2, mean_1_2, mean_2_2, scale_1_2, scale_2_2), dim=1)
            context_out = context_out.reshape(context_out.shape[0], 9, self.chs * 4, context_out.shape[-2], context_out.shape[-1])
            xq1 = torch.cat((torch.cat(Demultiplexerv2(xq1[:, :channel_num // 2, :, :]), dim=1),
                                torch.cat(Demultiplexerv2(xq1[:, channel_num // 2:, :, :]), dim=1)), dim=1)
            p3 = self.gaussin_entropy_func(xq1, context_out)
            return p3, context_out
        elif mode == 'forward':
            prob_1_0, mean_1_0, scale_1_0, prob_1_1, mean_1_1, scale_1_1, prob_1_2, mean_1_2, scale_1_2 = \
                [torch.chunk(param_out_1, 9, dim=1)[i].squeeze(1) for i in range(9)]
            prob_2_0, mean_2_0, scale_2_0, prob_2_1, mean_2_1, scale_2_1, prob_2_2, mean_2_2, scale_2_2 = \
                [torch.chunk(param_out_2, 9, dim=1)[i].squeeze(1) for i in range(9)]
            context_out = torch.cat((prob_1_0, prob_2_0, mean_1_0, mean_2_0, scale_1_0, scale_2_0,
                                    prob_1_1, prob_2_1, mean_1_1, mean_2_1, scale_1_1, scale_2_1,
                                    prob_1_2, prob_2_2, mean_1_2, mean_2_2, scale_1_2, scale_2_2), dim=1)
            context_out = context_out.reshape(context_out.shape[0], 9, self.chs, context_out.shape[-2], context_out.shape[-1])
            p3 = self.gaussin_entropy_func(xq1, context_out)
            return p3, context_out

    def forward(self,
                xq1_res: torch.Tensor,
                xq1: torch.Tensor,
                hyper: torch.Tensor,
                x1_predict: torch.Tensor) -> Tuple[Tensor, Tensor]:
        
        return self._compress_decompress(mode='forward', xq1=xq1_res, params=hyper, x1_predict=x1_predict)

    def compress(self,
                 xq1: torch.Tensor,
                 params: torch.Tensor,
                 x1_predict: torch.Tensor) -> Tuple[Tensor, Tensor]:
        return self._compress_decompress(mode='encode', xq1=xq1, params=params, x1_predict=x1_predict)

    def decompress(self, header, params, x1_predict, threshold, dec=None):
        return self._compress_decompress(mode='decode', xq1=None, params=params, x1_predict=x1_predict, header=header, threshold=threshold, dec=dec)

class Weighted_Gaussian_res(nn.Module):
    def __init__(self, M):
        super(Weighted_Gaussian_res, self).__init__()
        self.conv1 = MaskConv3d('A', 1, 24, 11, 1, 5)

        self.conv2 = nn.Sequential(nn.Conv3d(25, 48, 1, 1, 0), nn.ReLU(), nn.Conv3d(48, 96, 1, 1, 0), nn.ReLU(),
                                   nn.Conv3d(96, 9, 1, 1, 0))
        self.conv3 = nn.Conv2d(M * 2, M, 3, 1, 1)

        self.gaussin_entropy_func = Distribution_for_entropy2()

    def forward(self, xq1_res, xq1, hyper):
        x = torch.unsqueeze(xq1, dim=1)
        hyper = torch.unsqueeze(self.conv3(hyper), dim=1)
        x1 = self.conv1(x)
        output = self.conv2(torch.cat((x1, hyper), dim=1))
        p3 = self.gaussin_entropy_func(xq1_res, output)
        return p3, output


class Weighted_Gaussian_res_2mask(nn.Module):
    def __init__(self, M):
        super(Weighted_Gaussian_res_2mask, self).__init__()
        self.conv1 = MaskConv3d('A', 1, 24, 11, 1, 5)

        self.conv2 = nn.Sequential(nn.Conv3d(25, 48, 1, 1, 0), nn.ReLU(), nn.Conv3d(48, 96, 1, 1, 0), nn.ReLU(),
                                   nn.Conv3d(96, 9, 1, 1, 0))
        self.conv3 = nn.Conv2d(M * 2, M, 3, 1, 1)

        self.gaussin_entropy_func = Distribution_for_entropy2()

        ## added
        self.conv4 = MaskConv3d('C', 1, 24, 11, 1, 5)

    def forward(self, xq1_res, xq1, hyper, x1_predict):
        x = torch.unsqueeze(xq1, dim=1)
        hyper = torch.unsqueeze(self.conv3(hyper), dim=1)
        x1 = self.conv1(x)

        ## mask2 for prediction
        x1_1 = self.conv4(torch.unsqueeze(x1_predict, dim=1))
        x1 = x1 + x1_1

        output = self.conv2(torch.cat((x1, hyper), dim=1))
        p3 = self.gaussin_entropy_func(xq1_res, output)
        return p3, output