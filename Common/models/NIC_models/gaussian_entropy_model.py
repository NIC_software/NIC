'''
    Contributors: Haojie Liu, Tong Chen, Chuanmin Jia
'''
import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as f

from .functional_class import NormalCDF

class Low_bound(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x):

        ctx.save_for_backward(x)
        x = torch.clamp(x, min=1e-6)
        return x

    @staticmethod
    def backward(ctx, g):
        x, = ctx.saved_tensors
        grad1 = g.clone()
        grad1[x < 1e-6] = 0
        pass_through_if = np.logical_or(
            x.cpu().numpy() >= 1e-6, g.cpu().numpy() < 0.0)
        t = torch.Tensor(pass_through_if + 0.0).to(grad1.device)

        return grad1 * t


class Distribution_for_entropy(nn.Module):
    def __init__(self):
        super(Distribution_for_entropy, self).__init__()
        self.cdf = NormalCDF()

    def forward(self, x, p_dec):
        c = p_dec.size()[1]
        mean = p_dec[:, :c // 2, :, :]
        scale = p_dec[:, c // 2:, :, :]

        # to make the scale always positive
        scale[scale == 0] = 1e-9
        scale = torch.abs(scale)

        likelihood = torch.abs(self.cdf(x + 0.5, scale, mean) - self.cdf(x - 0.5, scale, mean))

        likelihood = Low_bound.apply(likelihood)
        return likelihood

class Distribution_for_entropy2(nn.Module):
    def __init__(self):
        super(Distribution_for_entropy2, self).__init__()
        self.softmax = nn.Softmax(dim=-1)
        self.cdf = NormalCDF()

    def forward(self, x, p_dec):
        # you can use use 3 gaussian
        prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = [
            torch.chunk(p_dec, 9, dim=1)[i].squeeze(1) for i in range(9)]
        # keep the weight  summation of prob == 1
        probs = torch.stack([prob0, prob1, prob2], dim=-1)

        probs = self.softmax(probs)
        scale0 = Low_bound.apply(torch.abs(scale0))
        scale1 = Low_bound.apply(torch.abs(scale1))
        scale2 = Low_bound.apply(torch.abs(scale2))

        # # 3 gaussian distribution

        likelihood0 = torch.abs(self.cdf(x + 0.5, scale0, mean0) - self.cdf(x - 0.5, scale0, mean0))
        likelihood1 = torch.abs(self.cdf(x + 0.5, scale1, mean1) - self.cdf(x - 0.5, scale1, mean1))
        likelihood2 = torch.abs(self.cdf(x + 0.5, scale2, mean2) - self.cdf(x - 0.5, scale2, mean2))


        likelihoods = Low_bound.apply(
            probs[:, :, :, :, 0] * likelihood0 + probs[:, :, :, :, 1] * likelihood1 + probs[:, :, :, :, 2] * likelihood2)

        return likelihoods


class Laplace_for_entropy(nn.Module):
    def __init__(self):
        super(Laplace_for_entropy, self).__init__()

    def forward(self, x, p_dec):
        mean = p_dec[:, 0, :, :, :]
        scale = p_dec[:, 1, :, :, :]

        # to make the scale always positive
        scale[scale == 0] = 1e-9
        # scale1 = torch.clamp(scale1,min = 1e-6)
        m1 = torch.distributions.laplace.Laplace(mean, scale)

        lower = m1.cdf(x - 0.5)
        upper = m1.cdf(x + 0.5)
        likelihood = torch.abs(upper - lower)

        likelihood = Low_bound.apply(likelihood)
        return likelihood
