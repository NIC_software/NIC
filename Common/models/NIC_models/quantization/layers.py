import re
import time
import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.functional as F

import warnings
from typing import Any, Callable, List, Optional, Tuple, Union

from .lut import *
from .functional import *
from ..functional_class import NormalCDF, Matmul, MM
from ..context_model import MultistageMaskedConv2d
from .quant_registry import nic_fake_quantized_layer


fake_quant = False
quant = True


# tensor multiplication-add
@nic_fake_quantized_layer(MM)
class FQMM(MM):
    def forward(self, x1: Tensor, x2: Tensor, bias: Tensor=None) -> Tensor:
        return qmm(x1, x2, bias, device=x1.device, quant=quant)

@nic_fake_quantized_layer(Matmul)
class FQMatmul(Matmul):
    def forward(self, x1: Tensor, x2: Tensor) -> Tensor:
        return qmatmul(x1, x2, device=x1.device, fake_quant=fake_quant, quant=quant)


# convolution
@nic_fake_quantized_layer(nn.Conv2d)
class FQConv(nn.Conv2d):
    def forward(self, input: Tensor) -> Tensor:
        return qconv(self, input, device=self.weight.device, fake_quant=fake_quant, quant=quant)

@nic_fake_quantized_layer(nn.ConvTranspose2d)
class FQConvTranspose(nn.ConvTranspose2d):
    def forward(self, input: Tensor) -> Tensor:
        return qtconv(self, input, device=self.weight.device, fake_quant=fake_quant, quant=quant)

@nic_fake_quantized_layer(MultistageMaskedConv2d)
class FQMultistageMaskedConv2d(MultistageMaskedConv2d):
    def forward(self, x: Tensor) -> Tensor:
        self.weight.data *= self.mask
        return qconv(self, x, device=self.weight.device, fake_quant=fake_quant, quant=quant)


# LUT-based
@nic_fake_quantized_layer(nn.Softmax)
class FQSoftmax(nn.Softmax):
    def forward(self, input: Tensor) -> Tensor:
        return softmax(input, dim=self.dim)
    
@nic_fake_quantized_layer(nn.GELU)
class FQGELU(nn.GELU):
    def forward(self, input: Tensor) -> Tensor:
        return gelu(input)
    
@nic_fake_quantized_layer(nn.Sigmoid)
class FQSigmoid(nn.Sigmoid):
    def forward(self, input: Tensor) -> Tensor:
        return sigmoid(input)

@nic_fake_quantized_layer(NormalCDF)
class QNormalCDF(nn.Module):
    def forward(self, x, std, mean):
        return cdf(x=x, scale=std, mean=mean)

        