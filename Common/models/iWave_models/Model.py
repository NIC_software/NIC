import torch

import Common.models.iWave_models.learn_wavelet_trans_additive as learn_wavelet_trans_additive
import Common.models.iWave_models.learn_wavelet_trans_affine as learn_wavelet_trans_affine
import Common.models.iWave_models.learn_wavelet_trans_lossless as learn_wavelet_trans_lossless
import Common.models.iWave_models.PixelCNN as PixelCNN
import Common.models.iWave_models.PixelCNN_lossless as PixelCNN_lossless
import Common.models.iWave_models.Quant as Quant
from Common.models.iWave_models.gan_post import GANPostProcessing
from Common.models.iWave_models.rcan import RCAN as PostProcessing

from Common.models.iWave_models.PixelCNN import GMM
from Common.models.iWave_models.PixelCNN_lossless import GMM_lossless, GMM_lossless_Context


class Transform_lossless(torch.nn.Module):
    def __init__(self, trainable_set=False):
        super(Transform_lossless, self).__init__()

        self.trans_steps = 4

        self.trainable_set = trainable_set

        self.wavelet_transform = torch.nn.ModuleList(
            learn_wavelet_trans_lossless.Wavelet(self.trainable_set) for _i in range(self.trans_steps))

    def forward_trans(self, x):
        LL = x
        HL_list = []
        LH_list = []
        HH_list = []
        for i in range(self.trans_steps):
            LL, HL, LH, HH = self.wavelet_transform[i].forward_trans(LL)
            HL_list.append(HL)
            LH_list.append(LH)
            HH_list.append(HH)

        return LL, HL_list, LH_list, HH_list

    def inverse_trans(self, LL, HL, LH, HH, layer):

        LL = self.wavelet_transform[layer].inverse_trans(LL, HL, LH, HH)

        return LL

class CodingLL_lossless(torch.nn.Module):
    def __init__(self):
        super(CodingLL_lossless, self).__init__()

        self.coding_LL = PixelCNN_lossless.PixelCNN()
        
        self.GMM = GMM_lossless()

    def forward(self, LL, lower_bound, upper_bound):
        params = self.coding_LL.inf_entro_parameters(LL)
        prob = self.GMM(params, lower_bound, upper_bound)
        
        return prob
    

class CodingHL_lossless(torch.nn.Module):
    def __init__(self):
        super(CodingHL_lossless, self).__init__()

        self.trans_steps = 4
        n = self.trans_steps
        self.coding_HL_list = torch.nn.ModuleList([PixelCNN_lossless.PixelCNN_Context(10, 4),PixelCNN_lossless.PixelCNN_Context(7, 3),PixelCNN_lossless.PixelCNN_Context(4, 2),PixelCNN_lossless.PixelCNN_Context(1, 2)])

        self.GMM = GMM_lossless_Context()
        
    def forward(self, context, lower_bound, upper_bound, layer, idx, split=0):
        params = self.coding_HL_list[layer].inf_entro_parameters(context, idx)
        prob = self.GMM(params, lower_bound, upper_bound, split)
        return prob


class CodingLH_lossless(torch.nn.Module):
    def __init__(self):
        super(CodingLH_lossless, self).__init__()

        self.trans_steps = 4
        n = self.trans_steps
        self.coding_LH_list = torch.nn.ModuleList([PixelCNN_lossless.PixelCNN_Context(11, 4),PixelCNN_lossless.PixelCNN_Context(8, 3),PixelCNN_lossless.PixelCNN_Context(5, 2),PixelCNN_lossless.PixelCNN_Context(2, 2)])

        self.GMM = GMM_lossless_Context()
        
    def forward(self, context, lower_bound, upper_bound, layer, idx, split=0):
        params = self.coding_LH_list[layer].inf_entro_parameters(context, idx)
        prob = self.GMM(params, lower_bound, upper_bound, split)
        
        return prob


class CodingHH_lossless(torch.nn.Module):
    def __init__(self):
        super(CodingHH_lossless, self).__init__()

        self.trans_steps = 4
        n = self.trans_steps
        self.coding_HH_list = torch.nn.ModuleList([PixelCNN_lossless.PixelCNN_Context(12, 4),PixelCNN_lossless.PixelCNN_Context(9, 3),PixelCNN_lossless.PixelCNN_Context(6, 2),PixelCNN_lossless.PixelCNN_Context(3, 2)])

        self.GMM = GMM_lossless_Context()
        
    def forward(self, context, lower_bound, upper_bound, layer, idx, split=0):
        params = self.coding_HH_list[layer].inf_entro_parameters(context, idx)
        prob = self.GMM(params, lower_bound, upper_bound, split)
        return prob


class Transform_aiWave(torch.nn.Module):
    def __init__(self, scale, isAffine, trainable_set=True):
        super(Transform_aiWave, self).__init__()

        self.trans_steps = 4

        self.trainable_set = trainable_set
        self.isAffine = isAffine

        if isAffine:
            self.wavelet_transform = torch.nn.ModuleList(
                learn_wavelet_trans_affine.Wavelet(trainable_set) for _i in range(self.trans_steps))
        else:
            self.wavelet_transform = learn_wavelet_trans_additive.Wavelet(trainable_set)

        self.ll_scale_net = Quant.Scale_net()
        self.hl_scale_net = Quant.Scale_net()
        self.lh_scale_net = Quant.Scale_net()
        self.hh_scale_net = Quant.Scale_net()
        self.scale_init = scale

    def forward_trans(self, x):

        ll_scale = float(self.trainable_set) * self.ll_scale_net() + self.scale_init
        hl_scale = float(self.trainable_set) * self.hl_scale_net() + self.scale_init
        lh_scale = float(self.trainable_set) * self.lh_scale_net() + self.scale_init
        hh_scale = float(self.trainable_set) * self.hh_scale_net() + self.scale_init

        LL = x
        HL_list = []
        LH_list = []
        HH_list = []
        for i in range(self.trans_steps):
            if self.isAffine:
                LL, HL, LH, HH = self.wavelet_transform[i].forward_trans(LL)
            else:
                LL, HL, LH, HH = self.wavelet_transform.forward_trans(LL)
            HL_list.append(torch.round(HL/hl_scale))
            LH_list.append(torch.round(LH/lh_scale))
            HH_list.append(torch.round(HH/hh_scale))

        LL = torch.round(LL/ll_scale)

        return LL, HL_list, LH_list, HH_list, ll_scale, hl_scale, lh_scale, hh_scale

    def inverse_trans(self, LL, HL, LH, HH, layer):

        if self.isAffine:
            LL = self.wavelet_transform[layer].inverse_trans(LL, HL, LH, HH)
        else:
            LL = self.wavelet_transform.inverse_trans(LL, HL, LH, HH)

        return LL

    def used_scale(self):
        ll_scale = float(self.trainable_set) * self.ll_scale_net() + self.scale_init
        hl_scale = float(self.trainable_set) * self.hl_scale_net() + self.scale_init
        lh_scale = float(self.trainable_set) * self.lh_scale_net() + self.scale_init
        hh_scale = float(self.trainable_set) * self.hh_scale_net() + self.scale_init
        return ll_scale, hl_scale, lh_scale, hh_scale


class CodingLL(torch.nn.Module):
    def __init__(self):
        super(CodingLL, self).__init__()

        self.coding_LL = PixelCNN.PixelCNN()
        
        self.GMM = GMM()

    def forward(self, LL, lower_bound, upper_bound):
        prob = self.coding_LL(LL, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, LL, lower_bound, upper_bound):
        params = self.coding_LL.inf_entro_parameters(LL)
        prob = self.GMM(params, lower_bound, upper_bound)       
        return prob


class CodingHL(torch.nn.Module):
    def __init__(self):
        super(CodingHL, self).__init__()

        self.trans_steps = 4

        self.coding_HL_list = torch.nn.ModuleList([PixelCNN.PixelCNN_Context(3*_i+1) for _i in reversed(range(self.trans_steps))])
        
        self.GMM = GMM()

    def forward(self, HL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HL_list[layer](HL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, HL, context, lower_bound, upper_bound, layer):
        params = self.coding_HL_list[layer].inf_entro_parameters(HL, context)
        prob = self.GMM(params, lower_bound, upper_bound)
        
        
        return prob


class CodingLH(torch.nn.Module):
    def __init__(self):
        super(CodingLH, self).__init__()

        self.trans_steps = 4

        self.coding_LH_list = torch.nn.ModuleList([PixelCNN.PixelCNN_Context(3*_i+2) for _i in reversed(range(self.trans_steps))])
        
        self.GMM = GMM()

    def forward(self, LH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LH_list[layer](LH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, LH, context, lower_bound, upper_bound, layer):
        params = self.coding_LH_list[layer].inf_entro_parameters(LH, context)
        prob = self.GMM(params, lower_bound, upper_bound)
        
        return prob



class CodingHH(torch.nn.Module):
    def __init__(self):
        super(CodingHH, self).__init__()

        self.trans_steps = 4

        self.coding_HH_list = torch.nn.ModuleList([PixelCNN.PixelCNN_Context(3*_i+3) for _i in reversed(range(self.trans_steps))])
        
        self.GMM = GMM()

    def forward(self, HH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HH_list[layer](HH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, HH, context, lower_bound, upper_bound, layer):
        params = self.coding_HH_list[layer].inf_entro_parameters(HH, context)
        prob = self.GMM(params, lower_bound, upper_bound)
        
        return prob


class Post(torch.nn.Module):
    def __init__(self):
        super(Post, self).__init__()

        self.post = PostProcessing(n_resgroups=10, n_resblocks=10, n_feats=32)

    def forward(self, x):

        post_recon = self.post(x)

        return post_recon


class PostGAN(torch.nn.Module):
    def __init__(self):
        super(PostGAN, self).__init__()

        self.gan_post = GANPostProcessing()

    def forward(self, x):

        post_recon = self.gan_post(x)

        return post_recon
