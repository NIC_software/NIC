import torch
import torch.nn as nn
import torch.distributions as dist

class MM(nn.Module):
    def forward(self, x, y, bias=None):
        return x*y if bias is None else x*y+bias

class Matmul(nn.Module):
    def forward(self, x, y):
        return torch.matmul(x, y)

class NormalCDF(nn.Module):
    def forward(self, x, std=1.0, mean=0.0):
        # Calculate the CDF for each element in x
        normal_dist = dist.Normal(mean, std)
        cdf_values = normal_dist.cdf(x)
        return cdf_values