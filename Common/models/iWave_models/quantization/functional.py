import re
import time
import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.functional as F

import warnings
from typing import Any, Callable, List, Optional, Tuple, Union

quant_weight_param = {}
quant_bias_param = {}


f'''

Args:
    conv: nn.COnv2d with weight and bias;
    x: input;
    bit: quantization bit-width
    device: the device to conv
    fake_quant: if device is cuda or fake_quant is True, the conv will use FP32 to conv
                (255.0)
    stride:
    kernel:
    quant: whether to quantize conv

'''

def qmm(x1: torch.Tensor, x2: torch.Tensor, bias: torch.Tensor=None, bit: int=16, 
            device: str='cuda', quant: bool=True) -> Tensor:
    # note: cuda supprots integer MM.
    if quant is False:
        return x1 * x2 if bias is None else x1 * x2 + bias

    bit_range = 2 ** (bit - 1)

    if bias is None:
        x1_int, r_x1 = Quant_Activation(x1, bit, device)
        x2_int, r_x2 = Quant_Activation(x2, bit, device)

        x1_int = x1_int.type(torch.int64)
        x2_int = x2_int.type(torch.int64)
            
        out_int = x1_int * x2_int
        
    else:
        x1_int, r_x1 = Quant_Activation(x1, bit, device)
        x2_int, r_x2 = Quant_Activation(x2, bit, device)
        b_int, r_b = Quant_Bias(bias, bit, device)

        x1_int = x1_int.type(torch.int64)
        x2_int = x2_int.type(torch.int64)
        b_int = torch.round(b_int * r_b * bit_range / r_x1 / r_x2)
        b_int = b_int.type(torch.int64)
            
        out_int = x1_int * x2_int + b_int

    out_hat = out_int.float() * r_x1 * r_x2 / bit_range / bit_range
    return out_hat

def qmatmul(x1: torch.Tensor, x2: torch.Tensor, bit: int=16, 
            device: str='cuda', fake_quant: bool=False, quant: bool=True) -> Tensor:
    
    if quant is False:
        return torch.matmul(x1, x2)

    bit_range = 2 ** (bit - 1)

    x1_int, r_x1 = Quant_Activation(x1, bit, device)
    x2_int, r_x2 = Quant_Activation(x2, bit, device)
    
    if fake_quant is False: 

        x1_int = x1_int.cpu().type(torch.int64)
        x2_int = x2_int.cpu().type(torch.int64)
        
        out_int = torch.matmul(x1_int, x2_int)

        out_int = out_int.to(device)
        out_hat = out_int.float() * r_x1 * r_x2 / bit_range / bit_range

    else: 

        out_int = torch.matmul(x1_int, x2_int)
        
        out_int = out_int.to(device)
        out_hat = out_int.float() * r_x1 * r_x2 / bit_range / bit_range

    return out_hat


def qconv(conv: nn.Conv2d, x: torch.Tensor, bit: int=12, device: str='cuda', 
          fake_quant: bool=False, quant: bool=True) -> Tensor:
    
    if quant is False:
        return F.conv2d(x, conv.weight, conv.bias, stride=conv.stride, padding=conv.padding, groups=conv.groups)

    #! change bit from 16 to 12
    bit_range = 1<<(bit-1)
    if conv.weight[0][0][0][0].item() in quant_weight_param:
        w_int, r_w = quant_weight_param[conv.weight[0][0][0][0].item()]
    else:
        w_int, r_w = Quant_Weight(conv.weight, bit, device)
        quant_weight_param[conv.weight[0][0][0][0].item()] = [w_int, r_w]
    if conv.bias[0].item() in quant_bias_param:
        b_int, r_b = quant_bias_param[conv.bias[0].item()]
    else:
        b_int, r_b = Quant_Bias(conv.bias, bit, device)
        quant_bias_param[conv.bias[0].item()] = [b_int, r_b]
    
    x_int, r_x = Quant_Activation(x, bit, device)
    
    if fake_quant is False: 
        w_int = w_int.type(torch.int32)
        b_int = torch.round(b_int * r_b * bit_range / torch.squeeze(r_w) / torch.squeeze(r_x))
        b_int = b_int.type(torch.int32)
        x_int = x_int.type(torch.int32)
        out_int = F.conv2d(x_int.type(torch.float64), w_int.type(torch.float64), b_int.type(torch.float64), stride=conv.stride, padding=conv.padding, groups=conv.groups)
        out_int = out_int.type(torch.int64)
        out_hat = (torch.mul(out_int.float(), r_w.transpose(0,1))) * r_x / bit_range / bit_range
    else: 
        w_int = w_int.cpu().type(torch.int32).float()
        b_int = torch.round(b_int * r_b * bit_range / torch.squeeze(r_w) / torch.squeeze(r_x))
        b_int = b_int.cpu().type(torch.int32).float()
        x_int = x_int.cpu().type(torch.int32).float()

        out_int = F.conv2d(x_int, w_int, b_int, stride=conv.stride, padding=conv.padding, groups=conv.groups)
        out_int = out_int.to(device)
        out_hat = (torch.mul(out_int.float(), r_w.transpose(0,1))) * r_x / bit_range / bit_range
    
    return out_hat

def qmaskconv(conv: nn.Conv2d, x: torch.Tensor, mask_type: str = "A", 
              bit: int=16, device: str='cuda', fake_quant: bool=False, 
              quant: bool=True) -> Tensor:
    
    
    if mask_type == 'A':
        conv.mask[:, :, 0::2, 0::2] = 1
    elif mask_type == 'B':
        conv.mask[:, :, 0::2, 1::2] = 1
        conv.mask[:, :, 1::2, 0::2] = 1
    elif mask_type == 'C':
        conv.mask[:, :, :, :] = 1
        conv.mask[:, :, 1:2, 1:2] = 0
    elif mask_type =='A/':
        conv.mask[:, :, 0::2, 1::2] = 1
        conv.mask[:, :, 1::2, 0::2] = 1
        conv.mask[:, :, 1, 1]=1
    elif mask_type =='B/':
        conv.mask[:, :, 0::2, 0::2] = 1
        conv.mask[:, :, 1, 1]=1
    elif mask_type =='C/':
        conv.mask[:, :, 1, 1]=1
    else:
        raise ValueError(f'Invalid "mask_type" value "{mask_type}"')

    if quant is False:
        return F.conv2d(x, conv.weight, conv.bias, stride=conv.stride, padding=conv.padding, groups=conv.groups)

    bit_range = 2 ** (bit - 1)

    w_int, r_w = Quant_Weight(conv.weight, bit, device)
    b_int, r_b = Quant_Bias(conv.bias, bit, device) # 10bit for bias rescaling
    x_int, s_x = Handle_Activation(x, 8, 8, device)
       
    if fake_quant is False: 

        w_int = w_int * conv.mask
        w_int = w_int.cpu().type(torch.int32)
        b_int = torch.round(b_int * r_b / torch.squeeze(r_w) * s_x)
        b_int = b_int.cpu().type(torch.int32)
        x_int = x_int.cpu().type(torch.int32)

        out_int = F.conv2d(x_int, w_int, b_int, stride=conv.stride, padding=conv.padding, groups=conv.groups)
        out_int = out_int.to(device)
        out_hat = torch.mul(out_int.float(), r_w.transpose(0,1)) / bit_range / s_x

    else: 
        w_int = w_int * conv.mask
        b_int = torch.round(b_int * r_b / torch.squeeze(r_w) * s_x)

        out_int = F.conv2d(x_int, w_int, b_int, stride=conv.stride, padding=conv.padding, groups=conv.groups)
        out_hat = torch.mul(out_int, r_w.transpose(0,1)) / bit_range / s_x

    return out_hat

def qtconv(conv: nn.Conv2d, x: torch.Tensor, bit: int=16, device: str='cuda', 
          fake_quant: bool=False, quant: bool=True) -> Tensor:
    
    if quant is False:
        return F.conv_transpose2d(x, conv.weight, conv.bias, stride=conv.stride, padding=conv.padding, 
                                  output_padding=conv.output_padding, groups=conv.groups)
    
    bit_range = 2 ** (bit - 1)

    w_int, r_w = Quant_Weight(conv.weight, bit, device, True)
    b_int, r_b = Quant_Bias(conv.bias, bit, device)
    x_int, s_x = Handle_Activation(x, 8, 8, device)

    if fake_quant is False:
        Warning(f'Pytorch does not support INT for transpose conv2d. Here force using fake quantization.')
        fake_quant = True

    w_int = w_int.cpu().type(torch.int32).float()
    b_int = torch.round(b_int * r_b / torch.squeeze(r_w) * s_x)
    b_int = b_int.cpu().type(torch.int32).float()
    x_int = x_int.cpu().type(torch.int32).float()

    out_int = F.conv_transpose2d(x_int, w_int, b_int, stride=conv.stride, padding=conv.padding, 
                                 output_padding=conv.output_padding, groups=conv.groups)
    out_int = out_int.to(device)
    out_hat = torch.mul(out_int.float(), r_w) / bit_range / s_x

    return out_hat

########################## Activation Quantization ###########################
### channel-wise
### symmetrical
### faster than for-loops
########################## Activation Quantization ###########################
def Quant_Activation(x: torch.Tensor, n_bits: int = 16, device: str='cuda', channel: bool=False):
    
    #! original version
    x = x.to(device)
    x_clone = x.clone().detach()

    eps = torch.tensor(1e-8, dtype=torch.float32)
    bit_range = 2 ** (n_bits - 1)

    if channel:
        range_float = x_clone.abs().max(dim=-1)[0].max(dim=-1)[0].max(dim=0)[0]
        range_int01 = torch.round(range_float * bit_range)
        range_int01 = torch.max(torch.tensor(1., device=range_int01.device), range_int01) / bit_range
        range_int01 = range_int01.view(1, -1, 1, 1)

        param_01 = torch.div(x_clone, range_int01)
        param_01 = torch.clamp(param_01, -1, 1)
        param_int = torch.round(param_01 * bit_range)

        return param_int, range_int01
    else: 
        range_float = x_clone.abs().max()
        range_int01 = torch.round(range_float * bit_range) 
        range_int01 = torch.max(torch.tensor(1., device=range_int01.device), range_int01) / bit_range

        param_01 = torch.clamp(x_clone / range_int01, -1, 1)
        param_int = torch.round(param_01 * bit_range)

        return param_int, range_int01

def Handle_Activation(x, a_l=8, a_r=8, device: str='cuda'):
    x = x.to(device)
    range = 2 ** (a_l + a_r - 1)
    a_mult = 2 ** a_r

    out = torch.clamp(torch.round(x * a_mult), -range, range)
    return out, a_mult

########################## Weight Quantization ###########################
### channel-wise
### symmetrical
### faster than for-loops
########################## Weight Quantization ###########################
def Quant_Weight(x: torch.Tensor, n_bits: int = 16, device: str='cuda', transpose: bool=False):
    
    #! original version
    x = x.to(device)
    x_clone = x.clone().detach()

    eps = torch.tensor(1e-8, dtype=torch.float32)
    # bit_range = 2 ** (n_bits - 1)
    bit_range = 1<<(n_bits-1)

    if transpose is False:
        range_float = x_clone.abs().max(dim=-1)[0].max(dim=-1)[0].max(dim=-1)[0]
        range_int01 = torch.round(range_float * bit_range) 
        range_int01 = torch.max(torch.tensor(1., device=range_int01.device), range_int01) / bit_range

        range_int01 = range_int01.view(-1, 1, 1, 1)

        param_01 = torch.div(x_clone, range_int01)
        param_01 = torch.clamp(param_01, -1, 1)
        param_int = torch.round(param_01 * bit_range)

    else:
        range_float = x_clone.abs().max(dim=-1)[0].max(dim=-1)[0].max(dim=0)[0]
        range_int01 = torch.round(range_float * bit_range)
        range_int01 = torch.max(torch.tensor(1., device=range_int01.device), range_int01) / bit_range
        
        range_int01 = range_int01.view(1, -1, 1, 1)

        param_01 = torch.div(x_clone, range_int01)
        param_01 = torch.clamp(param_01, -1, 1)
        param_int = torch.round(param_01 * bit_range)

    return param_int, range_int01

########################## Bias Quantization ###########################
### layer-wise
### symmetrical
### require bias rescaling
########################## Bias Quantization ###########################
def Quant_Bias(param, n_bits=16, device: str='cuda'):

    #! original version
    param = param.to(device)
    eps = torch.tensor(1e-8, dtype=torch.float32)

    bit_range = 1<<(n_bits - 1)

    range_float = param.abs().max()

    range_int01 = torch.round(range_float * bit_range)
    range_int01 = torch.max(torch.tensor(1., device=range_int01.device), range_int01) / bit_range

    param_01 = torch.clamp(param / range_int01, -1, 1)
    param_int = torch.round(param_01 * bit_range)

    return param_int, range_int01
