import os
import math
import torch
import torch.nn.functional as F

# def load_lut(funcname):
#     global lut
#     if not lut:
#         ROOTDIR = os.path.split(__file__)[0]
#         lut = torch.load(os.path.join(ROOTDIR, '..', 'lut', f"{funcname}.pt"))
#     return lut

lut = {}

def load_lut(funcname):
    global lut
    if funcname not in lut:
        ROOTDIR = os.path.split(__file__)[0]
        lut[funcname] = torch.load(os.path.join(ROOTDIR, '..', 'lut', f"{funcname}.pt"))
    return lut[funcname]


def tanh(x: torch.Tensor, scale_i: int = 1000, scale_v: int = 20000, 
         check: bool = False, eps: float=1e-3):

    lut = torch.load("./Common/models/NIC_models/lut/tanh.pt").to(x.device)
    index = torch.round(torch.clamp(x, -6, 6) * scale_i).type(torch.int64)
    sign = torch.sign(index)

    value = torch.gather(lut, 0, index.abs().reshape(-1)).reshape(index.shape)
    value = value / scale_v * sign

    if check:
        max_error = (value - torch.tanh(x)).max()
        if max_error > eps:
            Warning(f"The error is out of bound.")

    return value


def sigmoid(x: torch.Tensor, scale_i: int = 1000, scale_v: int = 20000, 
            check: bool = False, eps: float=1e-3):
    
    lut = load_lut('sigmoid').to(x.device)
    index = torch.round(torch.clamp(x, -10, 10) * scale_i).type(torch.int64)
    sign = torch.sign(index)

    value = torch.gather(lut, 0, index.abs().reshape(-1)).reshape(index.shape)
    # value = (value + 10000) / scale_v * sign 
    value = (value * sign + 10000) / scale_v

    if check:
        max_error = (value - torch.sigmoid(x)).max()
        if max_error > eps:
            Warning(f"The error is out of bound.")

    return value


def softmax(x: torch.Tensor, scale_i: int = 1000, scale_v: int = 20000, dim: int = -1, 
            check: bool = False, eps: float=1e-3):
    device = x.device
    x = x.cpu()
    
    lut = load_lut('exp').to(x.device)
    
    dim_max, _ = torch.max(x, dim=dim, keepdim=True)
    index = torch.round(torch.clamp(x-dim_max, -11, 0) * scale_i).type(torch.int64)
    index = index + 11000

    value = torch.gather(lut, 0, index.abs().reshape(-1)).reshape(index.shape)
    sum = torch.sum(value, dim,keepdim=True)
    value = torch.div(value, sum)
    if check:
        max_error = (value - torch.softmax(x, dim)).max()
        if max_error > eps:
            Warning(f"The error is out of bound.")
    value = value.to(device)

    return value


def cdf(x: torch.Tensor, scale: torch.Tensor, mean: torch.Tensor, 
        scale_i: int = 10000, scale_v: int = 100000, check: bool = False, eps: float=1e-3):
    
    # 240818
    device = x.device
    x = x.cpu()
    mean = mean.cpu()
    scale = scale.cpu()
    
    lut = load_lut('erf').to(x.device)
    x = (x - mean) * scale.reciprocal()
    index = torch.round(torch.clamp(x, -4.5, 4.5) * scale_i).type(torch.int64)
    sign = torch.sign(index)

    value = torch.gather(lut, 0, index.abs().reshape(-1)).reshape(index.shape)
    value = value / scale_v * sign 

    if check:
        max_error = (value - torch.erf(x / math.sqrt(2))).max()
        if max_error > eps:
            Warning(f"The error is out of bound.")

    # x = torch.clamp(x, -3, 3)
    # x = (x - mean) * scale.reciprocal() / math.sqrt(2)
    # x = (x - mean) * scale.reciprocal() * 0.70710678
    # value = torch.erf(x)

    value = 0.5 * (1 + value)
    
    value = value.to(device)
    
    return value


def gelu(x: torch.Tensor, scale_i: int = 10000, scale_v: int = 20000, 
         check: bool = False, eps: float=1e-3):
    
    lut = load_lut('gelu').to(x.device)
    
    index = torch.where((x>4.), 0., x.double())
    value0 = torch.where((x>4.), x.double(), 0.)
    value0 = torch.round(torch.clamp(value0, -2**16, 2**16) * scale_v).type(torch.int32)
    index = torch.round(torch.clamp(index, -4, 4) * scale_i).type(torch.int64)
    index += 40000
    value = torch.gather(lut, 0, index.reshape(-1)).reshape(index.shape)
    value = (value + value0) / scale_v

    # index = torch.round(torch.clamp(x, -4, 4) * scale_i).type(torch.int64)
    # index += 40000

    # value = torch.gather(lut, 0, index.reshape(-1)).reshape(index.shape)
    # value = value / scale_v

    if check:
        max_error = (value - F.gelu(x)).max()
        if max_error > eps:
            Warning(f"The error is out of bound.")

    return value