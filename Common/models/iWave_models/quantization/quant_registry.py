import torch.nn as nn

CUSTOM_QUANTIZATION_MODULES = dict()

def iwave_fake_quantized_layer(old_class):
    """
    Register a class as the quantization of another class

    Example:
    @register_quantization(OldClass)
    class QuantizedClass(nn.Module):
        ...
    """
    def _func(new_class):
        if old_class in CUSTOM_QUANTIZATION_MODULES:
            # if CUSTOM_QUANTIZATION_MODULES[old_class] is not new_class:
            #     raise ValueError(f"Another instance of quantization of class '{old_class}' already exists!")
            pass
        else:
            CUSTOM_QUANTIZATION_MODULES.setdefault(old_class, new_class)
        return new_class
    return _func

def iwave_quantize_module(module: nn.Module):
    for name, child in module.named_children():
        if type(child) in CUSTOM_QUANTIZATION_MODULES:
            child.__class__ = CUSTOM_QUANTIZATION_MODULES[type(child)]
        else:
            iwave_quantize_module(child)

def iwave_quantize_class(cls):
    """
    Mark a class.
    After initialization, the float layers will be replaced by corresponding quantized layers.
    """
    
    old__init__ = cls.__init__
    def new_init_(self, *args, **kwargs):
        # print(CUSTOM_QUANTIZATION_MODULES)
        # import pdb
        # pdb.set_trace()
        old__init__(self, *args, **kwargs)
        iwave_quantize_module(self)
    
    cls.__init__ = new_init_
    return cls

def show_classes(model: nn.Module):
    module_classes = set()
    for _, module in model.named_modules():
        module_classes.add(type(module))
    return module_classes

# @classmethod
# def echo(cls, mod):
#     return mod

# def nic_quantization_ignore(cls):
#     """
#     Mark that the class and all its submodules does not need quantization. Used in main encoder and main decoder
#     """

#     """
#     The modules are marked to be not quantized when initializing
#     """
#     old_init = cls.__init__
#     def new_init(self, *args, **kwargs):
#         old_init(self, *args, **kwargs)
#         for module in self.modules():
#             module.qconfig = None
#     cls.__init__ = new_init

#     CUSTOM_QUANTIZATION_MODULES.setdefault(cls, cls)
#     cls.from_observed = echo
#     return cls

# # Bugfix for pytorch. *_*||
# # The load_state_dict for quantized layers is not working when state dict mismatches

# def ignore_keyerror(old_func):
#     def new_func(*args, **kwargs):
#         try:
#             old_func(*args, **kwargs)
#         except KeyError:
#             pass
#     return new_func

# _ConvNd._load_from_state_dict = ignore_keyerror(_ConvNd._load_from_state_dict)
# Qnn.QFunctional._load_from_state_dict = ignore_keyerror(Qnn.QFunctional._load_from_state_dict)
# Qnn.ConvTranspose2d.from_observed = Qnn.ConvTranspose2d.from_float