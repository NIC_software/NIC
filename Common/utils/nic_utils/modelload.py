import torch

def load_state_dict(net, state_dict, verbose=True, **kwargs):
    sd = net.state_dict()
    for skey in sd:
        if skey in state_dict and state_dict[skey].shape == sd[skey].shape:
            sd[skey] = state_dict[skey]
        elif verbose and skey not in state_dict:
            raise RuntimeError(f"NOT load {skey}, not find it in state_dict")
        elif verbose:
            raise RuntimeError(
                f"NOT load {skey}, this {sd[skey].shape}, load {state_dict[skey].shape}"
            )
    net.load_state_dict(sd, **kwargs)