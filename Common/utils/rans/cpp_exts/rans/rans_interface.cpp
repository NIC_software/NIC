// Copyright (c) 2021-2022, InterDigital Communications, Inc
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted (subject to the limitations in the disclaimer
// below) provided that the following conditions are met:

// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// * Neither the name of InterDigital Communications, Inc nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
// THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
// CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
// NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
// OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/* Rans64 extensions from:
 * https://fgiesen.wordpress.com/2015/12/21/rans-in-practice/
 * Unbounded range coding from:
 * https://github.com/tensorflow/compression/blob/master/tensorflow_compression/cc/kernels/unbounded_index_range_coding_kernels.cc
 **/

#include "rans_interface.hpp"

#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <algorithm>
#include <array>
#include <cassert>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

#include "rans64.h"

namespace py = pybind11;

/* probability range, this could be a parameter... */
constexpr int precision = 16;

constexpr uint16_t bypass_precision = 4; /* number of bits in bypass mode */
constexpr uint16_t max_bypass_val = (1 << bypass_precision) - 1;

namespace {

/* We only run this in debug mode as its costly... */
void assert_cdfs(const py::array_t<int> &cdfs,
                 const py::array_t<int> &cdfs_sizes) {
  for (int i = 0; i < static_cast<int>(cdfs.size()); ++i) {
    assert(cdfs.at(i, 0) == 0);
    assert(cdfs.at(i, cdfs_sizes.at(i) - 1) == (1 << precision));
    for (int j = 0; j < cdfs_sizes.at(i) - 1; ++j) {
      assert(cdfs.at(i, j + 1) > cdfs.at(i, j));
    }
  }
}

/* Support only 16 bits word max */
inline void Rans64EncPutBits(Rans64State *r, uint32_t **pptr, uint32_t val,
                             uint32_t nbits) {
  assert(nbits <= 16);
  assert(val < (1u << nbits));

  /* Re-normalize */
  uint64_t x = *r;
  uint32_t freq = 1 << (16 - nbits);
  uint64_t x_max = ((RANS64_L >> 16) << 32) * freq;
  if (x >= x_max) {
    *pptr -= 1;
    **pptr = (uint32_t)x;
    x >>= 32;
    Rans64Assert(x < x_max);
  }

  /* x = C(s, x) */
  *r = (x << nbits) | val;
}

inline uint32_t Rans64DecGetBits(Rans64State *r, uint32_t **pptr,
                                 uint32_t n_bits) {
  uint64_t x = *r;
  uint32_t val = x & ((1u << n_bits) - 1);

  /* Re-normalize */
  x = x >> n_bits;
  if (x < RANS64_L) {
    x = (x << 32) | **pptr;
    *pptr += 1;
    Rans64Assert(x >= RANS64_L);
  }

  *r = x;

  return val;
}
}  // namespace

void NICRansEncoder::encode_with_indexes(
    const py::array_t<int32_t> &symbols, const py::array_t<int32_t> &indexes,
    const std::vector<py::array_t<int32_t>> &cdfs,
    const py::array_t<int32_t> &cdfs_sizes,
    const py::array_t<int32_t> &offsets) {
  assert(cdfs.size() == cdfs_sizes.size());

  // backward loop on symbols from the end;
  for (unsigned int i = 0; i < symbols.shape()[0]; ++i) {
    const int32_t cdf_idx = indexes.at(i);
    assert(cdf_idx >= 0);
    assert(cdf_idx < cdfs.size());

    const auto &cdf = cdfs[cdf_idx];

    const int32_t max_value = cdfs_sizes.at(cdf_idx) - 2;
    assert(max_value >= 0);
    assert((max_value + 1) < cdf.size());

    int32_t value = symbols.at(i) - offsets.at(cdf_idx);

    uint32_t raw_val = 0;
    if (value < 0) {
      raw_val = -2 * value - 1;
      value = max_value;
    } else if (value >= max_value) {
      raw_val = 2 * (value - max_value);
      value = max_value;
    }

    assert(value >= 0);
    assert(value < cdfs_sizes.at(cdf_idx) - 1);

    auto low = static_cast<uint16_t>(cdf.at(value));
    auto rng = static_cast<uint16_t>(cdf.at(value + 1) - cdf.at(value));
    assert(rng != 0);

    _syms.push_back({low, rng, false});

    /* Bypass coding mode (value == max_value -> sentinel flag) */
    if (value == max_value) {
      /* Determine the number of bypasses (in bypass_precision size) needed
      to
       * encode the raw value. */
      int32_t n_bypass = 0;
      while ((raw_val >> (n_bypass * bypass_precision)) != 0) {
        ++n_bypass;
      }

      /* Encode number of bypasses */
      int32_t val = n_bypass;
      while (val >= max_bypass_val) {
        _syms.push_back({max_bypass_val, max_bypass_val + 1, true});
        val -= max_bypass_val;
      }
      _syms.push_back(
          {static_cast<uint16_t>(val), static_cast<uint16_t>(val + 1),
          true});

      /* Encode raw value */
      for (int32_t j = 0; j < n_bypass; ++j) {
        const int32_t val =
            (raw_val >> (j * bypass_precision)) & max_bypass_val;
        _syms.push_back(
            {static_cast<uint16_t>(val), static_cast<uint16_t>(val + 1),
            true});
      }
    }
  }
}

void NICRansEncoder::Encode(const uint32_t lower, const uint32_t upper) {
  assert(upper > lower);
  _syms.push_back({static_cast<uint16_t>(lower),
                   static_cast<uint16_t>(upper - lower), false});
}

void NICRansEncoder::Encode_Batch(const py::array_t<uint32_t> &lowers,
                               const py::array_t<uint32_t> &uppers) {
  for (int i = 0; i < static_cast<int>(lowers.size()); ++i) {
    this->Encode(lowers.at(i), uppers.at(i));
  }
}

void NICRansEncoder::Encode_Cdf(const uint32_t x,
                             const py::array_t<uint32_t> &cdf) {
  const uint32_t lower = cdf.at(x), upper = cdf.at(x + 1);
  this->Encode(lower, upper);
}

py::bytes NICRansEncoder::Flush() {
  Rans64State rans;
  Rans64EncInit(&rans);

  std::vector<uint32_t> output(_syms.size(), 0xCC);  // too much space ?
  uint32_t *ptr = output.data() + output.size();
  assert(ptr != nullptr);

  while (!_syms.empty()) {
    const RansSymbol sym = _syms.back();

    if (!sym.bypass) {
      Rans64EncPut(&rans, &ptr, sym.start, sym.range, precision);
    } else {
      // unlikely...
      Rans64EncPutBits(&rans, &ptr, sym.start, bypass_precision);
    }
    _syms.pop_back();
  }

  Rans64EncFlush(&rans, &ptr);

  const int nbytes =
      std::distance(ptr, output.data() + output.size()) * sizeof(uint32_t);
  return std::string(reinterpret_cast<char *>(ptr), nbytes);
}

int32_t NICRansDecoder::Decode(const py::array_t<uint32_t> &cdf) {
  const uint32_t cum_freq = Rans64DecGet(&_rans, precision);

  // const auto cdf_end = cdf.begin() + cdfs_sizes[cdf_idx];
  const auto it = std::find_if(cdf.data(), cdf.data() + cdf.size(),
                               [cum_freq](uint32_t v) { return v > cum_freq; });
  // assert(it != cdf.end() + 1);
  const uint32_t s = std::distance(cdf.data(), it) - 1;

  Rans64DecAdvance(&_rans, &_ptr, cdf.at(s), cdf.at(s + 1) - cdf.at(s),
                   precision);

  int32_t value = static_cast<int32_t>(s) + _data_min;

  return value;
}

py::array_t<int32_t> NICRansDecoder::Decode_Batch(
    const py::array_t<uint32_t> &cdfs) {
  int output_length = static_cast<int>(cdfs.shape()[0]);
  py::array_t<int32_t> output(output_length);
  for (int i = 0; i < output_length; ++i) {
    const uint32_t cum_freq = Rans64DecGet(&_rans, precision);

    const auto cdf_begin = cdfs.data() + cdfs.index_at(i, 0);
    const auto cdf_end = cdf_begin + cdfs.shape()[1];
    const auto it = std::find_if(
        cdf_begin, cdf_end, [cum_freq](uint32_t v) { return v > cum_freq; });
    assert(it != cdf_end + 1);
    const uint32_t s = std::distance(cdf_begin, it) - 1;

    Rans64DecAdvance(&_rans, &_ptr, cdfs.at(i, s),
                     cdfs.at(i, s + 1) - cdfs.at(i, s), precision);

    int32_t value = static_cast<int32_t>(s) + _data_min;

    output.mutable_data()[i] = value;
  }
  return output;
}

void NICRansDecoder::Init(const std::string &encoded, const int32_t data_min,
                       const int32_t data_max) {
  _stream = encoded;
  _data_min = data_min;
  _data_max = data_max;
  uint32_t *ptr = (uint32_t *)_stream.data();
  assert(ptr != nullptr);
  _ptr = ptr;
  Rans64DecInit(&_rans, &_ptr);
}

py::array_t<int32_t> NICRansDecoder::decode_with_indexes(
    const std::string &encoded, const py::array_t<int32_t> &indexes,
    const std::vector<py::array_t<int32_t>> &cdfs,
    const py::array_t<int32_t> &cdfs_sizes,
    const py::array_t<int32_t> &offsets) {
  assert(cdfs.size() == cdfs_sizes.size());

  py::array_t<int32_t> output(indexes.size());

  Rans64State rans;
  uint32_t *ptr = (uint32_t *)encoded.data();
  assert(ptr != nullptr);
  Rans64DecInit(&rans, &ptr);

  for (int i = 0; i < static_cast<int>(indexes.size()); ++i) {
    const int32_t cdf_idx = indexes.at(i);
    assert(cdf_idx >= 0);
    assert(cdf_idx < cdfs.size());

    const auto &cdf = cdfs[cdf_idx];

    const int32_t max_value = cdfs_sizes.at(cdf_idx) - 2;
    assert(max_value >= 0);
    assert((max_value + 1) < cdf.size());

    const int32_t offset = offsets.at(cdf_idx);

    const uint32_t cum_freq = Rans64DecGet(&rans, precision);

    const auto cdf_begin = cdf.data();
    const auto cdf_end = cdf_begin + cdfs_sizes.at(cdf_idx);
    const auto it = std::find_if(
        cdf_begin, cdf_end, [cum_freq](uint32_t v) { return v > cum_freq; });
    assert(it != cdf_end + 1);
    const uint32_t s = std::distance(cdf_begin, it) - 1;

    Rans64DecAdvance(&rans, &ptr, cdf.at(s), cdf.at(s + 1) - cdf.at(s),
                     precision);

    int32_t value = static_cast<int32_t>(s);

    if (value == max_value) {
      /* Bypass decoding mode */
      int32_t val = Rans64DecGetBits(&rans, &ptr, bypass_precision);
      int32_t n_bypass = val;

      while (val == max_bypass_val) {
        val = Rans64DecGetBits(&rans, &ptr, bypass_precision);
        n_bypass += val;
      }

      int32_t raw_val = 0;
      for (int j = 0; j < n_bypass; ++j) {
        val = Rans64DecGetBits(&rans, &ptr, bypass_precision);
        assert(val <= max_bypass_val);
        raw_val |= val << (j * bypass_precision);
      }
      value = raw_val >> 1;
      if (raw_val & 1) {
        value = -value - 1;
      } else {
        value += max_value;
      }
    }

    output.mutable_data()[i] = value + offset;
  }

  return output;
}

py::array_t<int32_t> NICRansDecoder::decode_stream(
    const py::array_t<int32_t> &indexes,
    const std::vector<py::array_t<int32_t>> &cdfs,
    const py::array_t<int32_t> &cdfs_sizes,
    const py::array_t<int32_t> &offsets) {
  assert(cdfs.size() == cdfs_sizes.size());

  py::array_t<int32_t> output(indexes.size());

  assert(_ptr != nullptr);

  for (int i = 0; i < static_cast<int>(indexes.size()); ++i) {
    const int32_t cdf_idx = indexes.at(i);
    assert(cdf_idx >= 0);
    assert(cdf_idx < cdfs.size());

    const auto &cdf = cdfs[cdf_idx];

    const int32_t max_value = cdfs_sizes.at(cdf_idx) - 2;
    assert(max_value >= 0);
    assert((max_value + 1) < cdf.size());

    const int32_t offset = offsets.at(cdf_idx);

    const uint32_t cum_freq = Rans64DecGet(&_rans, precision);

    const auto cdf_begin = cdf.data();
    const auto cdf_end = cdf_begin + cdfs_sizes.at(cdf_idx);
    const auto it = std::find_if(
        cdf_begin, cdf_end, [cum_freq](uint32_t v) { return v > cum_freq; });
    assert(it != cdf_end + 1);
    const uint32_t s = std::distance(cdf_begin, it) - 1;

    Rans64DecAdvance(&_rans, &_ptr, cdf.at(s), cdf.at(s + 1) - cdf.at(s),
                     precision);

    int32_t value = static_cast<int32_t>(s);

    if (value == max_value) {
      /* Bypass decoding mode */
      int32_t val = Rans64DecGetBits(&_rans, &_ptr, bypass_precision);
      int32_t n_bypass = val;

      while (val == max_bypass_val) {
        val = Rans64DecGetBits(&_rans, &_ptr, bypass_precision);
        n_bypass += val;
      }

      int32_t raw_val = 0;
      for (int j = 0; j < n_bypass; ++j) {
        val = Rans64DecGetBits(&_rans, &_ptr, bypass_precision);
        assert(val <= max_bypass_val);
        raw_val |= val << (j * bypass_precision);
      }
      value = raw_val >> 1;
      if (raw_val & 1) {
        value = -value - 1;
      } else {
        value += max_value;
      }
    }

    output.mutable_data()[i] = value + offset;
  }

  return output;
}

PYBIND11_MODULE(rans, m) {
  m.attr("__name__") = "rans";

  m.doc() = "range Asymmetric Numeral System python bindings";

  py::class_<NICRansEncoder>(m, "RansEncoder")
      .def(py::init<>())
      .def("flush", &NICRansEncoder::Flush)
      .def("encode", &NICRansEncoder::Encode)
      .def("encode_cdf", &NICRansEncoder::Encode_Cdf)
      .def("encode_batch", &NICRansEncoder::Encode_Batch)
      .def("encode_with_indexes", &NICRansEncoder::encode_with_indexes);

  py::class_<NICRansDecoder>(m, "RansDecoder")
      .def(py::init<>())
      .def("init", &NICRansDecoder::Init)
      .def("decode", &NICRansDecoder::Decode)
      .def("decode_batch", &NICRansDecoder::Decode_Batch,
           "Decode a string to a list of symbols")
      .def("decode_stream", &NICRansDecoder::decode_stream)
      .def("decode_with_indexes", &NICRansDecoder::decode_with_indexes,
           "Decode a string to a list of symbols");
}
