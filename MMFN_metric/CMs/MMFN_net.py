import torch
import torch.nn as nn
from torch.nn import Parameter
import torch.nn.functional as F
from torch.nn.modules.module import Module
from torchvision import models


class Dist2LogitLayer(nn.Module):
    ''' takes 2 distances, puts through fc layers, spits out value between [0,1] (if use_sigmoid is True) '''

    def __init__(self, chn_mid=32, use_sigmoid=True):
        super(Dist2LogitLayer, self).__init__()

        layers = [nn.Conv2d(5, chn_mid, 1, stride=1, padding=0, bias=True), ]
        layers += [nn.LeakyReLU(0.2, True), ]
        layers += [nn.Conv2d(chn_mid, chn_mid, 1, stride=1, padding=0, bias=True), ]
        layers += [nn.LeakyReLU(0.2, True), ]
        layers += [nn.Conv2d(chn_mid, 1, 1, stride=1, padding=0, bias=True), ]
        if (use_sigmoid):
            layers += [nn.Sigmoid(), ]
        self.model = nn.Sequential(*layers)

    def forward(self, d0, d1, eps=0.1):
        return self.model.forward(torch.cat((d0, d1, d0 - d1, d0 / (d1 + eps), d1 / (d0 + eps)), dim=1))


class BaseModel(nn.Module):

    def __init__(self):
        super(BaseModel, self).__init__()

        res_net = models.resnet18(pretrained=False)
        self.feature = nn.Sequential(*list(res_net.children())[:-2])

        for p in self.parameters():
            p.requires_grad = False

        self.fcweight = nn.Sequential(
            nn.Dropout(0.3), nn.Linear(960, 10),
        )

        self.fcbias = nn.Sequential(
            nn.Dropout(0.3), nn.Linear(960, 10),
        )

        self.fc = nn.Sequential(
            nn.Linear(10, 64), nn.ReLU(inplace=True),
            nn.Dropout(0.3), nn.Linear(64, 1), nn.Sigmoid(),
        )

        self.bppfc = nn.Sequential(nn.Dropout(0.3), nn.Linear(64, 3))

        self.logitlayer = Dist2LogitLayer(chn_mid=32)


    def forward_once(self, disimg, refimg, metrics):

        f1 = self.feature[:5](disimg)
        f2 = self.feature[5](f1)
        f3 = self.feature[6](f2)
        f4 = self.feature[7](f3)

        x1 = torch.mean(f1, [2, 3])
        x2 = torch.mean(f2, [2, 3])
        x3 = torch.mean(f3, [2, 3])
        x4 = torch.mean(f4, [2, 3])

        x = torch.cat((x1, x2, x3, x4), dim=1)
        fc_w = self.fcweight(x)
        fc_b = self.fcbias(x)

        temp = self.fc[:2](fc_w*metrics+fc_b)
        s = self.fc[2:](temp)
        bpp = self.bppfc(temp)
        # print(temp.shape, s.shape, bpp.shape)

        return s, bpp

    def forward(self, refimg, imga, imgb, metricsa, metricsb, mode):

        if mode == 'class':
            _, bppa = self.forward_once(imga, refimg, metricsa)
            return bppa
        elif mode == 'pair':
            da, _ = self.forward_once(imga, refimg, metricsa)
            db, _ = self.forward_once(imgb, refimg, metricsb)
            da = da.unsqueeze(2).unsqueeze(3)
            db = db.unsqueeze(2).unsqueeze(3)
            prob = self.logitlayer(da, db)
            return prob
        else:
            da, _ = self.forward_once(imga, refimg, metricsa)
            db, _ = self.forward_once(imgb, refimg, metricsb)
            return da, db
