import numpy as np
import pandas as pd 
import sys
import os
import torch
import math
import utils
import cv2
sys.path.append('./CMs')
import argparse
from CMs import SSIM, GMSD, MS_SSIM, FSIM, VSI, NLPD, DISTS, LPIPSvgg, psnr,vif_p, MMFN_net

class MMFN():
    def __init__(self,resume='cps/MMFN_cp.pth'):
        self.model=MMFN_net.BaseModel()
        self.model=torch.nn.DataParallel(self.model)
        self.D_ssim,self.D_ms_ssim,self.D_gmsd,self.D_fsim,self.D_vsi,self.D_nlpd,self.D_lpipsvgg,self.D_dists=SSIM(channels=3),MS_SSIM(channels=3),GMSD(channels=3),FSIM(channels=3),VSI(channels=3),NLPD(channels=3),LPIPSvgg(channels=3),DISTS(channels=3)
        self.model=self.model.cuda()
        self.D_ssim,self.D_ms_ssim,self.D_gmsd,self.D_fsim,self.D_vsi,self.D_nlpd,self.D_lpipsvgg,self.D_dists=self.D_ssim.cuda(),self.D_ms_ssim.cuda(),self.D_gmsd.cuda(),self.D_fsim.cuda(),self.D_vsi.cuda(),self.D_nlpd.cuda(),self.D_lpipsvgg.cuda(),self.D_dists.cuda()
        utils.load_model(self.model,resume)
        self.model.eval()
    def cal_metrics(self,distimg,refimg):
        psnr_socres=psnr(distimg,refimg,reduction='none').unsqueeze(1)
        ssim_scores=self.D_ssim(distimg,refimg,as_loss=True).unsqueeze(1)
        ms_ssim_scores=self.D_ms_ssim(distimg,refimg,as_loss=True).unsqueeze(1)
        gmsd_scores=self.D_gmsd(distimg,refimg,as_loss=True).unsqueeze(1)
        fsim_scores=self.D_fsim(distimg,refimg,as_loss=True).unsqueeze(1)
        vsi_scores=self.D_vsi(distimg,refimg,as_loss=True).unsqueeze(1)
        nlpd_scores=self.D_nlpd(distimg,refimg,as_loss=True).unsqueeze(1)
        lpipsvgg_scores=self.D_lpipsvgg(distimg,refimg,as_loss=True).unsqueeze(1)
        dists_scores=self.D_dists(distimg,refimg,as_loss=True)
        if refimg.shape[0]==1:
            dists_scores=dists_scores.unsqueeze(0)
        dists_scores=dists_scores.unsqueeze(1)
        vifp_scores=vif_p(distimg,refimg,reduction='none').unsqueeze(1)
        metrics=torch.cat((psnr_socres,ssim_scores,ms_ssim_scores,gmsd_scores,fsim_scores,vsi_scores,nlpd_scores,lpipsvgg_scores,dists_scores,vifp_scores),1)
        #print(metrics)
        return metrics
    def predict_score(self,distimg,refimg,as_loss=False):
        refimg=torch.from_numpy(refimg).unsqueeze(0)
        distimg=torch.from_numpy(distimg).unsqueeze(0)
        refimg=refimg.cuda()
        distimg=distimg.cuda()
        metrics=self.cal_metrics(distimg,refimg)
        if as_loss:
            score,_=self.model.module.forward_once(distimg,refimg,metrics)
        else:
            with torch.no_grad():
                score,_=self.model.module.forward_once(distimg,refimg,metrics)
        score=score.cpu().item()
        return score


if __name__=='__main__':
    parser = argparse.ArgumentParser('args')
    parser.add_argument('--r_image', type=str, default='')
    parser.add_argument('--d_image', type=str, default='')
    parser.add_argument('--gpu_id', type=str, default='0')
    args = parser.parse_args()
    #print(args.r_image)
    #print(args.d_image)
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_id
    ref_img=utils.load_img(args.r_image)
    dis_img=utils.load_img(args.d_image)
    metric=MMFN()
    score=metric.predict_score(dis_img,ref_img)
    print(score)
