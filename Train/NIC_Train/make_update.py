"""
    Update the CDF in factorized model in case it's not updated
    Contributors: Xinyu Hang    
"""
import argparse
import os
import sys
import torch

import Common.models.NIC_models.model as model
sys.path.append(".")


USE_MULTI_HYPER = True
USE_PREDICTOR = True

def main(args):

    # load weights
    for weight in args.weights:
        # try:
        # build model
        image_comp = model.Image_coding_multi_hyper_res(3, args.M, args.N2, args.M, args.M // 2, False).cuda()
        # print(list(image_comp.state_dict().keys()))
        # raise ValueError()
        checkpoint = torch.load(weight, map_location="cpu")
        checkpoint = {k.replace('module.', ''): v for k, v in checkpoint.items()}
        image_comp.load_state_dict(
            checkpoint, strict=False
        )
        image_comp.factorized_entropy_func.updated = False
        image_comp.factorized_entropy_func.update()

        checkpoint = {'module.'+k: v for k, v in image_comp.state_dict().items()}
        
        assert(image_comp.factorized_entropy_func.updated)
        print(f"Dumping weights for weight: {weight}")
        torch.save(checkpoint, weight)
        del image_comp
        # except RuntimeError:
        #     print("Model loading failed for weight:", weight)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--M", type=int, default=192, help="the value of M")
    parser.add_argument("--N2", type=int, default=128, help="the value of N2")
    parser.add_argument("weights", type=str, nargs='*')

    args = parser.parse_args()
    print(args)
    main(args)
