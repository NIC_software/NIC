import os
import yaml
import time
import glob
import torch
import random
import logging
import argparse
import numpy as np
import torch.distributed as dist
from torch import nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import sys
from torchvision import transforms
from PIL import Image

from Model.postprocess_model import Restormer    
from Util.common import print_wt, avg_loss

os.environ['CUDA_LAUNCH_BLOCKING'] = '1'

class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, truth_glob, fake_glob, transform):
        super(CustomDataset, self).__init__()
        self.transform = transform

        self.truth_reader = glob.glob(truth_glob)
        self.fake_reader = glob.glob(fake_glob)
        self.truth_reader.sort()
        self.fake_reader.sort()

    def __getitem__(self, idx):
        truth_img = Image.open(self.truth_reader[idx])
        truth_img = self.transform(truth_img)

        fake_img = np.load(self.fake_reader[idx])/255.
        fake_img = self.transform(fake_img)

        patchsize = 128
        H, W = truth_img.shape[1], truth_img.shape[2]
        start_H = torch.randint(0, H - patchsize, ())
        start_W = torch.randint(0, W - patchsize, ())

        patch_img = truth_img[:, start_H:start_H + patchsize, start_W:start_W + patchsize]
        patch_img_fake = fake_img[:, start_H:start_H + patchsize, start_W:start_W + patchsize]

        hflip = random.random() > 0.5
        vflip = random.random() > 0.5
        rot90 = random.random() > 0.5

        if hflip: patch_img = torch.flip(patch_img, dims=[2])
        if vflip: patch_img = torch.flip(patch_img, dims=[1])
        if rot90: patch_img = patch_img.permute(0, 2, 1)

        if hflip: patch_img_fake = torch.flip(patch_img_fake, dims=[2])
        if vflip: patch_img_fake = torch.flip(patch_img_fake, dims=[1])
        if rot90: patch_img_fake = patch_img_fake.permute(0, 2, 1)

        return patch_img, patch_img_fake, torch.Tensor([idx])

    def __len__(self):
        return 1600


parser = argparse.ArgumentParser()
parser.add_argument('--Lambda', type=str, default='0.001')
parser.add_argument('--global_QP', type=int, default=40)

parser.add_argument('--batch_size', type=int, default=3)
parser.add_argument('--learning_rate', type=float, default=1e-4)
parser.add_argument('--grad_accum', type=int, default=2)

parser.add_argument('--train_input_path', type=str, default='/path_to_train_input/')
parser.add_argument('--train_gt_path', type=str, default='/path_to_train_gt/')
parser.add_argument('--test_input_path', type=str, default='/path_to_test_input/')
parser.add_argument('--test_gt_path', type=str, default='/path_to_test_gt/')
parser.add_argument('--work_dir', type=str, default='./results/')
parser.add_argument('--report_interval', type=int, default=50)
parser.add_argument('--eval_per_epoch', type=int, default=1)

parser.add_argument('--local_rank', type=int, default=-1)
args = parser.parse_args()


if __name__ == '__main__':
    args.local_rank = int(os.environ['LOCAL_RANK'])

    dist.init_process_group(backend='nccl')
    torch.cuda.set_device(args.local_rank)

    print_wt('Process No.{} starts.'.format(args.local_rank), only_0=False)

    _lambda = args.Lambda

    model = Restormer()

    print_wt('Start to build model.')
    best_model_name = args.work_dir+f'iWave_{_lambda}_best.ckpt'
    if os.path.exists(best_model_name):
        state_dict = torch.load(best_model_name, map_location=torch.device('cpu'))
        print_wt('Model loaded from {}.'.format(best_model_name))
        model.load_state_dict(state_dict, strict=False)
    model = model.cuda()
    model = torch.nn.parallel.DistributedDataParallel(
        model,
        device_ids=[args.local_rank],
        output_device=args.local_rank,
        broadcast_buffers=False,
        find_unused_parameters=True
    )

    truth_glob_path = os.path.join(args.train_gt_path, '*.png')
    fake_glob_path = os.path.join(args.train_input_path, '*.npy')
    print_wt('Start to build training dataset.')
    train_data = CustomDataset(
        truth_glob=truth_glob_path,
        fake_glob=fake_glob_path,
        transform=transforms.Compose([
            transforms.ToTensor()
        ])
    )
    train_sampler = torch.utils.data.distributed.DistributedSampler(
        train_data, rank=args.local_rank
    )
    train_loader = DataLoader(train_data, batch_size=args.batch_size, sampler=train_sampler,
                              shuffle=False, num_workers=4)

    print_wt('Start to load test data.')

    test_gt = []
    test_fake = []
    test_fake_png=[]
    test_truth_glob_path = os.path.join(args.test_gt_path, '*.png')
    total_test_paths = glob.glob(test_truth_glob_path)
    total_test_paths.sort()
    test_paths = total_test_paths[args.local_rank::dist.get_world_size()]

    test_fake_glob_path = os.path.join(args.test_input_path, '*.npy')
    total_test_fake_paths = glob.glob(test_fake_glob_path)
    total_test_fake_paths.sort()
    test_fake_paths = total_test_fake_paths[args.local_rank::dist.get_world_size()]
    
    test_fake_glob_path_png = os.path.join(args.test_input_path, '*.png')
    total_test_fake_paths_png = glob.glob(test_fake_glob_path_png)
    total_test_fake_paths_png.sort()
    test_fake_paths_png = total_test_fake_paths_png[args.local_rank::dist.get_world_size()]

    tv = transforms.ToTensor()
    
    num=len(test_paths)
    for i in range(num // dist.get_world_size()):
        img_gt = tv(Image.open(test_paths[i])).cuda()
        test_gt.append(img_gt)
        img_fake = tv(Image.open(test_fake_paths_png[i])).cuda()
        npy_fake = tv(np.load(test_fake_paths[i])/255.).cuda()
        test_fake.append(npy_fake)
        test_fake_png.append(img_fake)
    dist.barrier()

    if args.grad_accum > 1:
        print_wt('Using grad accumulation.')
    print_wt('Total batch size is {}*{}.'.format(args.batch_size * dist.get_world_size(), args.grad_accum))

    model_name = os.path.join(args.work_dir,f'iWave_{_lambda}.ckpt')
    opt_name = os.path.join(args.work_dir,f'iWave_{_lambda}_opt.ckpt')
    sch_name = os.path.join(args.work_dir,f'iWave_{_lambda}_sch.ckpt')
    epoch_name=os.path.join(args.work_dir,f'iWave_{_lambda}_epoch.npy')

    opt = torch.optim.Adam(model.parameters(), lr=args.learning_rate, betas=(0.9, 0.99))
    if os.path.exists(opt_name):
        opt.load_state_dict(torch.load(opt_name, map_location='cpu'))

    sch = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(opt, T_0=5, T_mult=2, eta_min=1e-6)
    if os.path.exists(sch_name):
        sch.load_state_dict(torch.load(sch_name, map_location='cpu'))
        
    if os.path.exists(epoch_name):
        start_epoch = np.load(epoch_name).item()
    else:
        start_epoch = 0

    print_wt('Start to train.')
    epoch_mse = 0
    epoch_baseline_mse = 0
    max_test_psnr = -1
    model.train()
    for epoch in range(start_epoch, 2500):
        start_time = time.time()
        torch.cuda.empty_cache()
        print_wt('Epoch {} starts.'.format(epoch + 1))
        train_sampler.set_epoch(epoch + 1)
        model.train()
        epoch_mse = 0
        epoch_baseline_mse = 0
        epoch_iterations = 1
        last_time = time.time()
        opt.zero_grad()
        for step, data in enumerate(train_loader):

            data_truth, data_fake, img_index = data
            data_truth = torch.Tensor(data_truth).cuda()
            data_fake = torch.Tensor(data_fake).cuda()
            rec_y = model(data_fake)

            loss = torch.sqrt((rec_y - data_truth) ** 2 + 1e-6).mean()
            loss = loss / args.grad_accum
            loss.backward()

            torch.nn.utils.clip_grad_norm_(model.parameters(), 10)
            for name, param in model.named_parameters():
                if param.grad is not None:
                    valid_gradients = not (torch.isnan(param.grad).any() or torch.isinf(param.grad).any())
                    if not valid_gradients:
                        print_wt('Grad overflow. Skip batch {}.'.format(epoch_iterations))
                        opt.zero_grad()
                        continue

            if epoch_iterations % args.grad_accum == 0:
                opt.step()
                opt.zero_grad()

            current_mse = F.mse_loss(rec_y, data_truth)
            epoch_mse += avg_loss(current_mse, dist.get_world_size()).item()
            if epoch_iterations % args.report_interval == 0:
                print_wt('{} / {} iterations. Loss is {}.'.format(epoch_iterations, len(train_loader), epoch_mse / epoch_iterations))
                curr_time = time.time()
                print_wt('{} secs per iteration.'.format((curr_time - start_time) / epoch_iterations))

            if np.isnan(current_mse.item()):
                print(img_index, "alert!!!!")
                exit()

            current_baseline_mse = F.mse_loss(data_fake, data_truth)
            epoch_baseline_mse += avg_loss(current_baseline_mse, dist.get_world_size())
            epoch_iterations += 1

        print_wt(f'TRAIN: avg mse:{epoch_mse / epoch_iterations} avg baseline_mse:{epoch_baseline_mse / epoch_iterations}')
        sch.step()  # cos T
        if (epoch + 1) % args.eval_per_epoch == 0:
            print_wt('Start to evaluate.')
            start_test_time = time.time()
            psnr = 0
            baseline_psnr = 0
            model.eval()
            for i in range(num // dist.get_world_size()):
                with torch.no_grad():
                    post_img = model(test_fake[i].unsqueeze(0))[0]
                    curr_psnr = 10 * torch.log10(1 / F.mse_loss(post_img, test_gt[i]))
                    curr_baseline_psnr = 10 * torch.log10(1 / F.mse_loss(test_fake_png[i], test_gt[i]))
                    psnr += avg_loss(curr_psnr, dist.get_world_size()) * dist.get_world_size()
                    baseline_psnr += avg_loss(curr_baseline_psnr, dist.get_world_size()) * dist.get_world_size()
            psnr /= num
            baseline_psnr /= num
            print_wt(f'TEST: psnr:{psnr} previous maxpsnr:{max_test_psnr} baseline:{baseline_psnr}')
            if psnr > max_test_psnr:
                max_test_psnr = psnr
                torch.save(model.module.state_dict(),
                           best_model_name)
                print_wt('Best model saved.')
            end_test_time = time.time()
            print_wt('Test ended in {} secs.'.format(end_test_time - start_test_time))
        torch.save(model.module.state_dict(),
                   model_name)
        torch.save(opt.state_dict(),
                   opt_name)
        torch.save(sch.state_dict(),
                   sch_name)
        np.save(epoch_name, epoch+1)
        print_wt('Newest model saved.')
        dist.barrier()
        print_wt(f"EPOCH {epoch + 1} time {time.time() - start_time}s:")
