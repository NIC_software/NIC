## Model Weights NIC

master: https://box.nju.edu.cn/f/de92b9a95f5a4687ae2b/

vbr models: https://rec.ustc.edu.cn/share/2f212a70-e3b0-11ee-9681-af3543323a31

history version:
NIM (last no entropy quantization version): https://box.nju.edu.cn/f/d88620ab374244bc9893/

NIM-1.0: https://box.nju.edu.cn/d/76b254dcde9c42d68830/

### Notes
1. update "lambdas_list" in NIC_preprocessing.py after modifying $\lambda$ during training.

2. config json file should align with pretrained models

## Model Weights iWave

https://rec.ustc.edu.cn/share/5fd424d0-89aa-11ee-b07a-83200d51fda7

## Model Weights BEE

https://pan.baidu.com/s/1euhK6bul5Ym7D9s5z7BKWg?pwd=l2ge 
